<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CSurveyAnswer
 *
 * @ORM\Table(name="c_survey_answer", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CSurveyAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="answer_id", type="integer", nullable=false)
     */
    private $answerId;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_id", type="integer", nullable=false)
     */
    private $surveyId;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=false)
     */
    private $questionId;

    /**
     * @var string
     *
     * @ORM\Column(name="option_id", type="text", length=0, nullable=false)
     */
    private $optionId;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=250, nullable=false)
     */
    private $user;


}

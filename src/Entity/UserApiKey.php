<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserApiKey
 *
 * @ORM\Table(name="user_api_key", indexes={@ORM\Index(name="idx_user_api_keys_user", columns={"user_id"})})
 * @ORM\Entity
 */
class UserApiKey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=32, nullable=false)
     */
    private $apiKey;

    /**
     * @var string
     *
     * @ORM\Column(name="api_service", type="string", length=10, nullable=false)
     */
    private $apiService;

    /**
     * @var string|null
     *
     * @ORM\Column(name="api_end_point", type="text", length=0, nullable=true)
     */
    private $apiEndPoint;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="validity_start_date", type="datetime", nullable=true)
     */
    private $validityStartDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="validity_end_date", type="datetime", nullable=true)
     */
    private $validityEndDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;


}

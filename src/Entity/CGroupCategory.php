<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CGroupCategory
 *
 * @ORM\Table(name="c_group_category", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CGroupCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="doc_state", type="boolean", nullable=false)
     */
    private $docState;

    /**
     * @var bool
     *
     * @ORM\Column(name="calendar_state", type="boolean", nullable=false)
     */
    private $calendarState;

    /**
     * @var bool
     *
     * @ORM\Column(name="work_state", type="boolean", nullable=false)
     */
    private $workState;

    /**
     * @var bool
     *
     * @ORM\Column(name="announcements_state", type="boolean", nullable=false)
     */
    private $announcementsState;

    /**
     * @var bool
     *
     * @ORM\Column(name="forum_state", type="boolean", nullable=false)
     */
    private $forumState;

    /**
     * @var bool
     *
     * @ORM\Column(name="wiki_state", type="boolean", nullable=false)
     */
    private $wikiState;

    /**
     * @var bool
     *
     * @ORM\Column(name="chat_state", type="boolean", nullable=false)
     */
    private $chatState;

    /**
     * @var int
     *
     * @ORM\Column(name="max_student", type="integer", nullable=false)
     */
    private $maxStudent;

    /**
     * @var bool
     *
     * @ORM\Column(name="self_reg_allowed", type="boolean", nullable=false)
     */
    private $selfRegAllowed;

    /**
     * @var bool
     *
     * @ORM\Column(name="self_unreg_allowed", type="boolean", nullable=false)
     */
    private $selfUnregAllowed;

    /**
     * @var int
     *
     * @ORM\Column(name="groups_per_user", type="integer", nullable=false)
     */
    private $groupsPerUser;

    /**
     * @var int
     *
     * @ORM\Column(name="display_order", type="integer", nullable=false)
     */
    private $displayOrder;


}

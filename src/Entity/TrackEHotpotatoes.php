<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackEHotpotatoes
 *
 * @ORM\Table(name="track_e_hotpotatoes", indexes={@ORM\Index(name="idx_tehp_user_id", columns={"exe_user_id"}), @ORM\Index(name="idx_tehp_c_id", columns={"c_id"})})
 * @ORM\Entity
 */
class TrackEHotpotatoes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="exe_name", type="string", length=255, nullable=false)
     */
    private $exeName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="exe_user_id", type="integer", nullable=true)
     */
    private $exeUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exe_date", type="datetime", nullable=false)
     */
    private $exeDate;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="exe_result", type="smallint", nullable=false)
     */
    private $exeResult;

    /**
     * @var int
     *
     * @ORM\Column(name="exe_weighting", type="smallint", nullable=false)
     */
    private $exeWeighting;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CForumMailcue
 *
 * @ORM\Table(name="c_forum_mailcue", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="thread", columns={"thread_id"}), @ORM\Index(name="user", columns={"user_id"}), @ORM\Index(name="post", columns={"post_id"})})
 * @ORM\Entity
 */
class CForumMailcue
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="thread_id", type="integer", nullable=false)
     */
    private $threadId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="post_id", type="integer", nullable=false)
     */
    private $postId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraFieldRelTag
 *
 * @ORM\Table(name="extra_field_rel_tag", indexes={@ORM\Index(name="field", columns={"field_id"}), @ORM\Index(name="item", columns={"item_id"}), @ORM\Index(name="tag", columns={"tag_id"}), @ORM\Index(name="field_item_tag", columns={"field_id", "item_id", "tag_id"})})
 * @ORM\Entity
 */
class ExtraFieldRelTag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="field_id", type="integer", nullable=false)
     */
    private $fieldId;

    /**
     * @var int
     *
     * @ORM\Column(name="tag_id", type="integer", nullable=false)
     */
    private $tagId;

    /**
     * @var int
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId;


}

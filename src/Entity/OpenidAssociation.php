<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OpenidAssociation
 *
 * @ORM\Table(name="openid_association")
 * @ORM\Entity
 */
class OpenidAssociation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="idp_endpoint_uri", type="text", length=0, nullable=false)
     */
    private $idpEndpointUri;

    /**
     * @var string
     *
     * @ORM\Column(name="session_type", type="string", length=30, nullable=false)
     */
    private $sessionType;

    /**
     * @var string
     *
     * @ORM\Column(name="assoc_handle", type="text", length=0, nullable=false)
     */
    private $assocHandle;

    /**
     * @var string
     *
     * @ORM\Column(name="assoc_type", type="text", length=0, nullable=false)
     */
    private $assocType;

    /**
     * @var int
     *
     * @ORM\Column(name="expires_in", type="bigint", nullable=false)
     */
    private $expiresIn;

    /**
     * @var string
     *
     * @ORM\Column(name="mac_key", type="text", length=0, nullable=false)
     */
    private $macKey;

    /**
     * @var int
     *
     * @ORM\Column(name="created", type="bigint", nullable=false)
     */
    private $created;


}

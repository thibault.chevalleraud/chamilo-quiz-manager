<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassItem
 *
 * @ORM\Table(name="class_item")
 * @ORM\Entity
 */
class ClassItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=40, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=0, nullable=false)
     */
    private $name;


}

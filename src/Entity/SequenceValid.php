<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceValid
 *
 * @ORM\Table(name="sequence_valid", indexes={@ORM\Index(name="IDX_F78B9CE655C65E08", columns={"sequence_variable_id"}), @ORM\Index(name="IDX_F78B9CE68C0A7083", columns={"sequence_condition_id"})})
 * @ORM\Entity
 */
class SequenceValid
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \SequenceVariable
     *
     * @ORM\ManyToOne(targetEntity="SequenceVariable")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_variable_id", referencedColumnName="id")
     * })
     */
    private $sequenceVariable;

    /**
     * @var \SequenceCondition
     *
     * @ORM\ManyToOne(targetEntity="SequenceCondition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_condition_id", referencedColumnName="id")
     * })
     */
    private $sequenceCondition;


}

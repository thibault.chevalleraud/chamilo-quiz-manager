<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserFriendRelationType
 *
 * @ORM\Table(name="user_friend_relation_type")
 * @ORM\Entity
 */
class UserFriendRelationType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=20, nullable=true)
     */
    private $title;


}

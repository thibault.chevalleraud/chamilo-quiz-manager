<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraFieldOptionRelFieldOption
 *
 * @ORM\Table(name="extra_field_option_rel_field_option", uniqueConstraints={@ORM\UniqueConstraint(name="idx", columns={"field_id", "role_id", "field_option_id", "related_field_option_id"})})
 * @ORM\Entity
 */
class ExtraFieldOptionRelFieldOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="field_option_id", type="integer", nullable=true)
     */
    private $fieldOptionId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="related_field_option_id", type="integer", nullable=true)
     */
    private $relatedFieldOptionId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="role_id", type="integer", nullable=true)
     */
    private $roleId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="field_id", type="integer", nullable=true)
     */
    private $fieldId;


}

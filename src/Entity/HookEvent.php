<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HookEvent
 *
 * @ORM\Table(name="hook_event", uniqueConstraints={@ORM\UniqueConstraint(name="class_name", columns={"class_name"})})
 * @ORM\Entity
 */
class HookEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="class_name", type="string", length=255, nullable=true)
     */
    private $className;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


}

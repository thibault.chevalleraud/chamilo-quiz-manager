<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CourseRelUserCatalogue
 *
 * @ORM\Table(name="course_rel_user_catalogue", indexes={@ORM\Index(name="IDX_79CA412EA76ED395", columns={"user_id"}), @ORM\Index(name="IDX_79CA412E91D79BD3", columns={"c_id"})})
 * @ORM\Entity
 */
class CourseRelUserCatalogue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="visible", type="integer", nullable=false)
     */
    private $visible;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="c_id", referencedColumnName="id")
     * })
     */
    private $c;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CAttendanceCalendarRelGroup
 *
 * @ORM\Table(name="c_attendance_calendar_rel_group", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="group", columns={"group_id"})})
 * @ORM\Entity
 */
class CAttendanceCalendarRelGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @var int
     *
     * @ORM\Column(name="calendar_id", type="integer", nullable=false)
     */
    private $calendarId;


}

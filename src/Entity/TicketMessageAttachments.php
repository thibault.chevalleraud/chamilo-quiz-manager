<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketMessageAttachments
 *
 * @ORM\Table(name="ticket_message_attachments", indexes={@ORM\Index(name="IDX_70BF9E26700047D2", columns={"ticket_id"}), @ORM\Index(name="IDX_70BF9E26537A1329", columns={"message_id"})})
 * @ORM\Entity
 */
class TicketMessageAttachments
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="text", length=0, nullable=false)
     */
    private $filename;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @var int
     *
     * @ORM\Column(name="sys_insert_user_id", type="integer", nullable=false)
     */
    private $sysInsertUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sys_insert_datetime", type="datetime", nullable=false)
     */
    private $sysInsertDatetime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sys_lastedit_user_id", type="integer", nullable=true)
     */
    private $sysLasteditUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sys_lastedit_datetime", type="datetime", nullable=true)
     */
    private $sysLasteditDatetime;

    /**
     * @var \TicketMessage
     *
     * @ORM\ManyToOne(targetEntity="TicketMessage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="message_id", referencedColumnName="id")
     * })
     */
    private $message;

    /**
     * @var \TicketTicket
     *
     * @ORM\ManyToOne(targetEntity="TicketTicket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ticket_id", referencedColumnName="id")
     * })
     */
    private $ticket;


}

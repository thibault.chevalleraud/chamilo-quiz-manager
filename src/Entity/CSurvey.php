<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CSurvey
 *
 * @ORM\Table(name="c_survey", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CSurvey
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_id", type="integer", nullable=false)
     */
    private $surveyId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", length=0, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subtitle", type="text", length=0, nullable=true)
     */
    private $subtitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="author", type="string", length=20, nullable=true)
     */
    private $author;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lang", type="string", length=20, nullable=true)
     */
    private $lang;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="avail_from", type="date", nullable=true)
     */
    private $availFrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="avail_till", type="date", nullable=true)
     */
    private $availTill;

    /**
     * @var string|null
     *
     * @ORM\Column(name="is_shared", type="string", length=1, nullable=true)
     */
    private $isShared;

    /**
     * @var string|null
     *
     * @ORM\Column(name="template", type="string", length=20, nullable=true)
     */
    private $template;

    /**
     * @var string|null
     *
     * @ORM\Column(name="intro", type="text", length=0, nullable=true)
     */
    private $intro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surveythanks", type="text", length=0, nullable=true)
     */
    private $surveythanks;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @var int
     *
     * @ORM\Column(name="invited", type="integer", nullable=false)
     */
    private $invited;

    /**
     * @var int
     *
     * @ORM\Column(name="answered", type="integer", nullable=false)
     */
    private $answered;

    /**
     * @var string
     *
     * @ORM\Column(name="invite_mail", type="text", length=0, nullable=false)
     */
    private $inviteMail;

    /**
     * @var string
     *
     * @ORM\Column(name="reminder_mail", type="text", length=0, nullable=false)
     */
    private $reminderMail;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_subject", type="string", length=255, nullable=false)
     */
    private $mailSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="anonymous", type="string", length=10, nullable=false)
     */
    private $anonymous;

    /**
     * @var string|null
     *
     * @ORM\Column(name="access_condition", type="text", length=0, nullable=true)
     */
    private $accessCondition;

    /**
     * @var bool
     *
     * @ORM\Column(name="shuffle", type="boolean", nullable=false)
     */
    private $shuffle;

    /**
     * @var bool
     *
     * @ORM\Column(name="one_question_per_page", type="boolean", nullable=false)
     */
    private $oneQuestionPerPage;

    /**
     * @var string
     *
     * @ORM\Column(name="survey_version", type="string", length=255, nullable=false)
     */
    private $surveyVersion;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_type", type="integer", nullable=false)
     */
    private $surveyType;

    /**
     * @var int
     *
     * @ORM\Column(name="show_form_profile", type="integer", nullable=false)
     */
    private $showFormProfile;

    /**
     * @var string
     *
     * @ORM\Column(name="form_fields", type="text", length=0, nullable=false)
     */
    private $formFields;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="visible_results", type="integer", nullable=true)
     */
    private $visibleResults;


}

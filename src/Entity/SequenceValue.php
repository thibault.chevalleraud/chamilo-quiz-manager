<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceValue
 *
 * @ORM\Table(name="sequence_value", indexes={@ORM\Index(name="IDX_66FBF12D218736B2", columns={"sequence_row_entity_id"})})
 * @ORM\Entity
 */
class SequenceValue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var float
     *
     * @ORM\Column(name="advance", type="float", precision=10, scale=0, nullable=false)
     */
    private $advance;

    /**
     * @var int
     *
     * @ORM\Column(name="complete_items", type="integer", nullable=false)
     */
    private $completeItems;

    /**
     * @var int
     *
     * @ORM\Column(name="total_items", type="integer", nullable=false)
     */
    private $totalItems;

    /**
     * @var bool
     *
     * @ORM\Column(name="success", type="boolean", nullable=false)
     */
    private $success;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="success_date", type="datetime", nullable=true)
     */
    private $successDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="available", type="boolean", nullable=false)
     */
    private $available;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="available_start_date", type="datetime", nullable=true)
     */
    private $availableStartDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="available_end_date", type="datetime", nullable=true)
     */
    private $availableEndDate;

    /**
     * @var \SequenceRowEntity
     *
     * @ORM\ManyToOne(targetEntity="SequenceRowEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_row_entity_id", referencedColumnName="id")
     * })
     */
    private $sequenceRowEntity;


}

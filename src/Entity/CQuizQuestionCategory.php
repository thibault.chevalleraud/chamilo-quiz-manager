<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuizQuestionCategory
 *
 * @ORM\Table(name="c_quiz_question_category", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CQuizQuestionCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkillRelUserComment
 *
 * @ORM\Table(name="skill_rel_user_comment", indexes={@ORM\Index(name="IDX_7AE9F6B6484A9317", columns={"skill_rel_user_id"}), @ORM\Index(name="IDX_7AE9F6B63AF3B65B", columns={"feedback_giver_id"}), @ORM\Index(name="idx_select_su_giver", columns={"skill_rel_user_id", "feedback_giver_id"})})
 * @ORM\Entity
 */
class SkillRelUserComment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback_text", type="text", length=0, nullable=false)
     */
    private $feedbackText;

    /**
     * @var int|null
     *
     * @ORM\Column(name="feedback_value", type="integer", nullable=true, options={"default"="1"})
     */
    private $feedbackValue = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="feedback_datetime", type="datetime", nullable=false)
     */
    private $feedbackDatetime;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feedback_giver_id", referencedColumnName="id")
     * })
     */
    private $feedbackGiver;

    /**
     * @var \SkillRelUser
     *
     * @ORM\ManyToOne(targetEntity="SkillRelUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="skill_rel_user_id", referencedColumnName="id")
     * })
     */
    private $skillRelUser;


}

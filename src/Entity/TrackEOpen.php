<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackEOpen
 *
 * @ORM\Table(name="track_e_open")
 * @ORM\Entity
 */
class TrackEOpen
{
    /**
     * @var int
     *
     * @ORM\Column(name="open_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $openId;

    /**
     * @var string
     *
     * @ORM\Column(name="open_remote_host", type="text", length=0, nullable=false)
     */
    private $openRemoteHost;

    /**
     * @var string
     *
     * @ORM\Column(name="open_agent", type="text", length=0, nullable=false)
     */
    private $openAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="open_referer", type="text", length=0, nullable=false)
     */
    private $openReferer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="open_date", type="datetime", nullable=false)
     */
    private $openDate;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CBlogTaskRelUser
 *
 * @ORM\Table(name="c_blog_task_rel_user", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user", columns={"user_id"}), @ORM\Index(name="task", columns={"task_id"})})
 * @ORM\Entity
 */
class CBlogTaskRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_id", type="integer", nullable=false)
     */
    private $blogId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="target_date", type="date", nullable=false)
     */
    private $targetDate;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="task_id", type="integer", nullable=false)
     */
    private $taskId;


}

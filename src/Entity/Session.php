<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})}, indexes={@ORM\Index(name="IDX_D044D5D4EE1F8395", columns={"session_category_id"}), @ORM\Index(name="idx_id_coach", columns={"id_coach"}), @ORM\Index(name="idx_id_session_admin_id", columns={"session_admin_id"})})
 * @ORM\Entity
 */
class Session
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="show_description", type="boolean", nullable=true)
     */
    private $showDescription;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbr_courses", type="smallint", nullable=true)
     */
    private $nbrCourses;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbr_users", type="integer", nullable=true)
     */
    private $nbrUsers;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbr_classes", type="integer", nullable=true)
     */
    private $nbrClasses;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_admin_id", type="integer", nullable=true)
     */
    private $sessionAdminId;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility", type="integer", nullable=false)
     */
    private $visibility;

    /**
     * @var int|null
     *
     * @ORM\Column(name="promotion_id", type="integer", nullable=true)
     */
    private $promotionId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="display_start_date", type="datetime", nullable=true)
     */
    private $displayStartDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="display_end_date", type="datetime", nullable=true)
     */
    private $displayEndDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="access_start_date", type="datetime", nullable=true)
     */
    private $accessStartDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="access_end_date", type="datetime", nullable=true)
     */
    private $accessEndDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="coach_access_start_date", type="datetime", nullable=true)
     */
    private $coachAccessStartDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="coach_access_end_date", type="datetime", nullable=true)
     */
    private $coachAccessEndDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="send_subscription_notification", type="boolean", nullable=false)
     */
    private $sendSubscriptionNotification = '0';

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_coach", referencedColumnName="id")
     * })
     */
    private $idCoach;

    /**
     * @var \SessionCategory
     *
     * @ORM\ManyToOne(targetEntity="SessionCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_category_id", referencedColumnName="id")
     * })
     */
    private $sessionCategory;


}

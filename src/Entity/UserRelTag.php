<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserRelTag
 *
 * @ORM\Table(name="user_rel_tag", indexes={@ORM\Index(name="idx_urt_uid", columns={"user_id"}), @ORM\Index(name="idx_urt_tid", columns={"tag_id"})})
 * @ORM\Entity
 */
class UserRelTag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="tag_id", type="integer", nullable=false)
     */
    private $tagId;


}

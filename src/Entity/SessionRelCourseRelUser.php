<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionRelCourseRelUser
 *
 * @ORM\Table(name="session_rel_course_rel_user", indexes={@ORM\Index(name="IDX_720167E613FECDF", columns={"session_id"}), @ORM\Index(name="idx_session_rel_course_rel_user_id_user", columns={"user_id"}), @ORM\Index(name="idx_session_rel_course_rel_user_course_id", columns={"c_id"})})
 * @ORM\Entity
 */
class SessionRelCourseRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility", type="integer", nullable=false)
     */
    private $visibility;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var int|null
     *
     * @ORM\Column(name="legal_agreement", type="integer", nullable=true)
     */
    private $legalAgreement;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     * })
     */
    private $session;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="c_id", referencedColumnName="id")
     * })
     */
    private $c;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

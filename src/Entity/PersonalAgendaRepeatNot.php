<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonalAgendaRepeatNot
 *
 * @ORM\Table(name="personal_agenda_repeat_not")
 * @ORM\Entity
 */
class PersonalAgendaRepeatNot
{
    /**
     * @var int
     *
     * @ORM\Column(name="cal_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $calId;

    /**
     * @var int
     *
     * @ORM\Column(name="cal_date", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $calDate;


}

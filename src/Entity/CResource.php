<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CResource
 *
 * @ORM\Table(name="c_resource", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CResource
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="source_type", type="string", length=50, nullable=true)
     */
    private $sourceType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="source_id", type="integer", nullable=true)
     */
    private $sourceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="resource_type", type="string", length=50, nullable=true)
     */
    private $resourceType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="resource_id", type="integer", nullable=true)
     */
    private $resourceId;


}

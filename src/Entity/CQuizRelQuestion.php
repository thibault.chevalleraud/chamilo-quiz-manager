<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuizRelQuestion
 *
 * @ORM\Table(name="c_quiz_rel_question", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="question", columns={"question_id"}), @ORM\Index(name="exercise", columns={"exercice_id"})})
 * @ORM\Entity
 */
class CQuizRelQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="question_order", type="integer", nullable=false)
     */
    private $questionOrder;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=false)
     */
    private $questionId;

    /**
     * @var int
     *
     * @ORM\Column(name="exercice_id", type="integer", nullable=false)
     */
    private $exerciceId;

    /**
     * @return int
     */
    public function getIid(): int
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid(int $iid): void
    {
        $this->iid = $iid;
    }

    /**
     * @return int
     */
    public function getCId(): int
    {
        return $this->cId;
    }

    /**
     * @param int $cId
     */
    public function setCId(int $cId): void
    {
        $this->cId = $cId;
    }

    /**
     * @return int
     */
    public function getQuestionOrder(): int
    {
        return $this->questionOrder;
    }

    /**
     * @param int $questionOrder
     */
    public function setQuestionOrder(int $questionOrder): void
    {
        $this->questionOrder = $questionOrder;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     */
    public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }

    /**
     * @return int
     */
    public function getExerciceId(): int
    {
        return $this->exerciceId;
    }

    /**
     * @param int $exerciceId
     */
    public function setExerciceId(int $exerciceId): void
    {
        $this->exerciceId = $exerciceId;
    }




}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="idx_message_user_sender", columns={"user_sender_id"}), @ORM\Index(name="idx_message_user_receiver", columns={"user_receiver_id"}), @ORM\Index(name="idx_message_user_sender_user_receiver", columns={"user_sender_id", "user_receiver_id"}), @ORM\Index(name="idx_message_user_receiver_status", columns={"user_receiver_id", "msg_status"}), @ORM\Index(name="idx_message_group", columns={"group_id"}), @ORM\Index(name="idx_message_parent", columns={"parent_id"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_sender_id", type="integer", nullable=false)
     */
    private $userSenderId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_receiver_id", type="integer", nullable=false)
     */
    private $userReceiverId;

    /**
     * @var bool
     *
     * @ORM\Column(name="msg_status", type="boolean", nullable=false)
     */
    private $msgStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_date", type="datetime", nullable=false)
     */
    private $sendDate;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=false)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="votes", type="integer", nullable=true)
     */
    private $votes;


}

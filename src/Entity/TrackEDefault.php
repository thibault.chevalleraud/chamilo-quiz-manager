<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackEDefault
 *
 * @ORM\Table(name="track_e_default", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session", columns={"session_id"})})
 * @ORM\Entity
 */
class TrackEDefault
{
    /**
     * @var int
     *
     * @ORM\Column(name="default_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $defaultId;

    /**
     * @var int
     *
     * @ORM\Column(name="default_user_id", type="integer", nullable=false)
     */
    private $defaultUserId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="c_id", type="integer", nullable=true)
     */
    private $cId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="default_date", type="datetime", nullable=true)
     */
    private $defaultDate;

    /**
     * @var string
     *
     * @ORM\Column(name="default_event_type", type="string", length=255, nullable=false)
     */
    private $defaultEventType;

    /**
     * @var string
     *
     * @ORM\Column(name="default_value_type", type="string", length=255, nullable=false)
     */
    private $defaultValueType;

    /**
     * @var string
     *
     * @ORM\Column(name="default_value", type="text", length=0, nullable=false)
     */
    private $defaultValue;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;


}

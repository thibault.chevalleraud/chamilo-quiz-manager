<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CTool
 *
 * @ORM\Table(name="c_tool", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CTool
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=false)
     */
    private $link;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visibility", type="boolean", nullable=true)
     */
    private $visibility;

    /**
     * @var string|null
     *
     * @ORM\Column(name="admin", type="string", length=255, nullable=true)
     */
    private $admin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="added_tool", type="boolean", nullable=true)
     */
    private $addedTool;

    /**
     * @var string
     *
     * @ORM\Column(name="target", type="string", length=20, nullable=false)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=20, nullable=false, options={"default"="authoring"})
     */
    private $category = 'authoring';

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="custom_icon", type="string", length=255, nullable=true)
     */
    private $customIcon;


}

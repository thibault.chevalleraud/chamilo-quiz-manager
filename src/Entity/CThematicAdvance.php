<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CThematicAdvance
 *
 * @ORM\Table(name="c_thematic_advance", indexes={@ORM\Index(name="IDX_62798E9754177093", columns={"room_id"}), @ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="thematic_id", columns={"thematic_id"})})
 * @ORM\Entity
 */
class CThematicAdvance
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="thematic_id", type="integer", nullable=false)
     */
    private $thematicId;

    /**
     * @var int
     *
     * @ORM\Column(name="attendance_id", type="integer", nullable=false)
     */
    private $attendanceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var bool
     *
     * @ORM\Column(name="done_advance", type="boolean", nullable=false)
     */
    private $doneAdvance;

    /**
     * @var \Room
     *
     * @ORM\ManyToOne(targetEntity="Room")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     * })
     */
    private $room;


}

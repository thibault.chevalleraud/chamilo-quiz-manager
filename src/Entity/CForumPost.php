<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CForumPost
 *
 * @ORM\Table(name="c_forum_post", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="poster_id", columns={"poster_id"}), @ORM\Index(name="forum_id", columns={"forum_id"}), @ORM\Index(name="idx_forum_post_thread_id", columns={"thread_id"}), @ORM\Index(name="idx_forum_post_visible", columns={"visible"})})
 * @ORM\Entity
 */
class CForumPost
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="post_id", type="integer", nullable=false)
     */
    private $postId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_title", type="string", length=250, nullable=true)
     */
    private $postTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="post_text", type="text", length=0, nullable=true)
     */
    private $postText;

    /**
     * @var int|null
     *
     * @ORM\Column(name="thread_id", type="integer", nullable=true)
     */
    private $threadId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_id", type="integer", nullable=true)
     */
    private $forumId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="poster_id", type="integer", nullable=true)
     */
    private $posterId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="poster_name", type="string", length=100, nullable=true)
     */
    private $posterName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="post_date", type="datetime", nullable=true)
     */
    private $postDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="post_notification", type="boolean", nullable=true)
     */
    private $postNotification;

    /**
     * @var int|null
     *
     * @ORM\Column(name="post_parent_id", type="integer", nullable=true)
     */
    private $postParentId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visible", type="boolean", nullable=true)
     */
    private $visible;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;


}

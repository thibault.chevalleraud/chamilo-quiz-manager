<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GradeComponents
 *
 * @ORM\Table(name="grade_components")
 * @ORM\Entity
 */
class GradeComponents
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="percentage", type="string", length=255, nullable=false)
     */
    private $percentage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="acronym", type="string", length=255, nullable=false)
     */
    private $acronym;

    /**
     * @var int
     *
     * @ORM\Column(name="grade_model_id", type="integer", nullable=false)
     */
    private $gradeModelId;


}

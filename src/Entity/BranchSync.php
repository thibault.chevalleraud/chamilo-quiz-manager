<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BranchSync
 *
 * @ORM\Table(name="branch_sync", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_F62F45EDE3C68343", columns={"unique_id"})}, indexes={@ORM\Index(name="IDX_F62F45ED727ACA70", columns={"parent_id"})})
 * @ORM\Entity
 */
class BranchSync
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="access_url_id", type="integer", nullable=false)
     */
    private $accessUrlId;

    /**
     * @var string
     *
     * @ORM\Column(name="unique_id", type="string", length=50, nullable=false)
     */
    private $uniqueId;

    /**
     * @var string
     *
     * @ORM\Column(name="branch_name", type="string", length=250, nullable=false)
     */
    private $branchName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="branch_ip", type="string", length=40, nullable=true)
     */
    private $branchIp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $latitude;

    /**
     * @var string|null
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $longitude;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dwn_speed", type="integer", nullable=true)
     */
    private $dwnSpeed;

    /**
     * @var int|null
     *
     * @ORM\Column(name="up_speed", type="integer", nullable=true)
     */
    private $upSpeed;

    /**
     * @var int|null
     *
     * @ORM\Column(name="delay", type="integer", nullable=true)
     */
    private $delay;

    /**
     * @var string|null
     *
     * @ORM\Column(name="admin_mail", type="string", length=250, nullable=true)
     */
    private $adminMail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="admin_name", type="string", length=250, nullable=true)
     */
    private $adminName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="admin_phone", type="string", length=250, nullable=true)
     */
    private $adminPhone;

    /**
     * @var int|null
     *
     * @ORM\Column(name="last_sync_trans_id", type="bigint", nullable=true)
     */
    private $lastSyncTransId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_sync_trans_date", type="datetime", nullable=true)
     */
    private $lastSyncTransDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_sync_type", type="string", length=20, nullable=true)
     */
    private $lastSyncType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ssl_pub_key", type="string", length=250, nullable=true)
     */
    private $sslPubKey;

    /**
     * @var string|null
     *
     * @ORM\Column(name="branch_type", type="string", length=250, nullable=true)
     */
    private $branchType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lft", type="integer", nullable=true)
     */
    private $lft;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rgt", type="integer", nullable=true)
     */
    private $rgt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="lvl", type="integer", nullable=true)
     */
    private $lvl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @var \BranchSync
     *
     * @ORM\ManyToOne(targetEntity="BranchSync")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;


}

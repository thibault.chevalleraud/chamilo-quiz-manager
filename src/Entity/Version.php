<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Version
 *
 * @ORM\Table(name="version", uniqueConstraints={@ORM\UniqueConstraint(name="version", columns={"version"})})
 * @ORM\Entity
 */
class Version
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="version", type="string", length=20, nullable=true)
     */
    private $version;


}

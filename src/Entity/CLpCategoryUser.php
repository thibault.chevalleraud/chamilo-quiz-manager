<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CLpCategoryUser
 *
 * @ORM\Table(name="c_lp_category_user", indexes={@ORM\Index(name="IDX_61F042712469DE2", columns={"category_id"}), @ORM\Index(name="IDX_61F0427A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class CLpCategoryUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CLpCategory
     *
     * @ORM\ManyToOne(targetEntity="CLpCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="iid")
     * })
     */
    private $category;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

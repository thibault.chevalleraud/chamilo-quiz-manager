<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraFieldValues
 *
 * @ORM\Table(name="extra_field_values", indexes={@ORM\Index(name="IDX_171DF924443707B0", columns={"field_id"}), @ORM\Index(name="idx_efv_fiii", columns={"field_id", "item_id"})})
 * @ORM\Entity
 */
class ExtraFieldValues
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var int
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     */
    private $itemId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var \ExtraField
     *
     * @ORM\ManyToOne(targetEntity="ExtraField")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;


}

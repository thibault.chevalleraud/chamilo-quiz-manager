<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessUrlRelSession
 *
 * @ORM\Table(name="access_url_rel_session")
 * @ORM\Entity
 */
class AccessUrlRelSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="access_url_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $accessUrlId;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sessionId;


}

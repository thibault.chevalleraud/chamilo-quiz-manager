<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CourseRelUser
 *
 * @ORM\Table(name="course_rel_user", indexes={@ORM\Index(name="IDX_92CFD9FEA76ED395", columns={"user_id"}), @ORM\Index(name="IDX_92CFD9FE91D79BD3", columns={"c_id"})})
 * @ORM\Entity
 */
class CourseRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="relation_type", type="integer", nullable=false)
     */
    private $relationType;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_tutor", type="boolean", nullable=true)
     */
    private $isTutor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort", type="integer", nullable=true)
     */
    private $sort;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_course_cat", type="integer", nullable=true)
     */
    private $userCourseCat;

    /**
     * @var int|null
     *
     * @ORM\Column(name="legal_agreement", type="integer", nullable=true)
     */
    private $legalAgreement;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="c_id", referencedColumnName="id")
     * })
     */
    private $c;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventSent
 *
 * @ORM\Table(name="event_sent", indexes={@ORM\Index(name="event_name_index", columns={"event_type_name"})})
 * @ORM\Entity
 */
class EventSent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_from", type="integer", nullable=false)
     */
    private $userFrom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_to", type="integer", nullable=true)
     */
    private $userTo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="event_type_name", type="string", length=100, nullable=true)
     */
    private $eventTypeName;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceFormula
 *
 * @ORM\Table(name="sequence_formula", indexes={@ORM\Index(name="IDX_533B9159B2D1386E", columns={"sequence_method_id"}), @ORM\Index(name="IDX_533B915955C65E08", columns={"sequence_variable_id"})})
 * @ORM\Entity
 */
class SequenceFormula
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \SequenceVariable
     *
     * @ORM\ManyToOne(targetEntity="SequenceVariable")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_variable_id", referencedColumnName="id")
     * })
     */
    private $sequenceVariable;

    /**
     * @var \SequenceMethod
     *
     * @ORM\ManyToOne(targetEntity="SequenceMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_method_id", referencedColumnName="id")
     * })
     */
    private $sequenceMethod;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketAssignedLog
 *
 * @ORM\Table(name="ticket_assigned_log", indexes={@ORM\Index(name="IDX_54B65868700047D2", columns={"ticket_id"}), @ORM\Index(name="IDX_54B65868A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class TicketAssignedLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="sys_insert_user_id", type="integer", nullable=false)
     */
    private $sysInsertUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="assigned_date", type="datetime", nullable=false)
     */
    private $assignedDate;

    /**
     * @var \TicketTicket
     *
     * @ORM\ManyToOne(targetEntity="TicketTicket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ticket_id", referencedColumnName="id")
     * })
     */
    private $ticket;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

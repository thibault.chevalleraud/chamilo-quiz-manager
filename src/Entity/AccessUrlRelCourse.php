<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessUrlRelCourse
 *
 * @ORM\Table(name="access_url_rel_course", indexes={@ORM\Index(name="IDX_8E97FC0891D79BD3", columns={"c_id"}), @ORM\Index(name="IDX_8E97FC0873444FD5", columns={"access_url_id"})})
 * @ORM\Entity
 */
class AccessUrlRelCourse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AccessUrl
     *
     * @ORM\ManyToOne(targetEntity="AccessUrl")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="access_url_id", referencedColumnName="id")
     * })
     */
    private $accessUrl;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="c_id", referencedColumnName="id")
     * })
     */
    private $c;


}

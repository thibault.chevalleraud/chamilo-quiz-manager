<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceRuleMethod
 *
 * @ORM\Table(name="sequence_rule_method", indexes={@ORM\Index(name="IDX_6336EA764044CA89", columns={"sequence_rule_id"}), @ORM\Index(name="IDX_6336EA76B2D1386E", columns={"sequence_method_id"})})
 * @ORM\Entity
 */
class SequenceRuleMethod
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="method_order", type="integer", nullable=false)
     */
    private $methodOrder;

    /**
     * @var \SequenceRule
     *
     * @ORM\ManyToOne(targetEntity="SequenceRule")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_rule_id", referencedColumnName="id")
     * })
     */
    private $sequenceRule;

    /**
     * @var \SequenceMethod
     *
     * @ORM\ManyToOne(targetEntity="SequenceMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_method_id", referencedColumnName="id")
     * })
     */
    private $sequenceMethod;


}

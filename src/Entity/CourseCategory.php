<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CourseCategory
 *
 * @ORM\Table(name="course_category", uniqueConstraints={@ORM\UniqueConstraint(name="code", columns={"code"})}, indexes={@ORM\Index(name="parent_id", columns={"parent_id"}), @ORM\Index(name="tree_pos", columns={"tree_pos"})})
 * @ORM\Entity
 */
class CourseCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=40, nullable=false)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_id", type="string", length=40, nullable=true)
     */
    private $parentId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tree_pos", type="integer", nullable=true)
     */
    private $treePos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="children_count", type="smallint", nullable=true)
     */
    private $childrenCount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="auth_course_child", type="string", length=40, nullable=true)
     */
    private $authCourseChild;

    /**
     * @var string|null
     *
     * @ORM\Column(name="auth_cat_child", type="string", length=40, nullable=true)
     */
    private $authCatChild;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CDropboxFile
 *
 * @ORM\Table(name="c_dropbox_file", uniqueConstraints={@ORM\UniqueConstraint(name="UN_filename", columns={"filename"})}, indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CDropboxFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="uploader_id", type="integer", nullable=false)
     */
    private $uploaderId;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=250, nullable=false)
     */
    private $filename;

    /**
     * @var int
     *
     * @ORM\Column(name="filesize", type="integer", nullable=false)
     */
    private $filesize;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=250, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=250, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="author", type="string", length=250, nullable=true)
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="upload_date", type="datetime", nullable=false)
     */
    private $uploadDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_upload_date", type="datetime", nullable=false)
     */
    private $lastUploadDate;

    /**
     * @var int
     *
     * @ORM\Column(name="cat_id", type="integer", nullable=false)
     */
    private $catId;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;


}

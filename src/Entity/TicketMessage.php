<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketMessage
 *
 * @ORM\Table(name="ticket_message", indexes={@ORM\Index(name="IDX_BA71692D700047D2", columns={"ticket_id"})})
 * @ORM\Entity
 */
class TicketMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string|null
     *
     * @ORM\Column(name="message", type="text", length=0, nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=255, nullable=false)
     */
    private $ipAddress;

    /**
     * @var int
     *
     * @ORM\Column(name="sys_insert_user_id", type="integer", nullable=false)
     */
    private $sysInsertUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sys_insert_datetime", type="datetime", nullable=false)
     */
    private $sysInsertDatetime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sys_lastedit_user_id", type="integer", nullable=true)
     */
    private $sysLasteditUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sys_lastedit_datetime", type="datetime", nullable=true)
     */
    private $sysLasteditDatetime;

    /**
     * @var \TicketTicket
     *
     * @ORM\ManyToOne(targetEntity="TicketTicket")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ticket_id", referencedColumnName="id")
     * })
     */
    private $ticket;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuiz
 *
 * @ORM\Table(name="c_quiz", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CQuiz
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sound", type="string", length=255, nullable=true)
     */
    private $sound;

    /**
     * @var bool
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="random", type="integer", nullable=false)
     */
    private $random;

    /**
     * @var bool
     *
     * @ORM\Column(name="random_answers", type="boolean", nullable=false)
     */
    private $randomAnswers;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var int
     *
     * @ORM\Column(name="results_disabled", type="integer", nullable=false)
     */
    private $resultsDisabled;

    /**
     * @var string|null
     *
     * @ORM\Column(name="access_condition", type="text", length=0, nullable=true)
     */
    private $accessCondition;

    /**
     * @var int
     *
     * @ORM\Column(name="max_attempt", type="integer", nullable=false)
     */
    private $maxAttempt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var int
     *
     * @ORM\Column(name="feedback_type", type="integer", nullable=false)
     */
    private $feedbackType;

    /**
     * @var int
     *
     * @ORM\Column(name="expired_time", type="integer", nullable=false)
     */
    private $expiredTime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="propagate_neg", type="integer", nullable=false)
     */
    private $propagateNeg;

    /**
     * @var bool
     *
     * @ORM\Column(name="save_correct_answers", type="boolean", nullable=false)
     */
    private $saveCorrectAnswers;

    /**
     * @var int
     *
     * @ORM\Column(name="review_answers", type="integer", nullable=false)
     */
    private $reviewAnswers;

    /**
     * @var int
     *
     * @ORM\Column(name="random_by_category", type="integer", nullable=false)
     */
    private $randomByCategory;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text_when_finished", type="text", length=0, nullable=true)
     */
    private $textWhenFinished;

    /**
     * @var int
     *
     * @ORM\Column(name="display_category_name", type="integer", nullable=false)
     */
    private $displayCategoryName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pass_percentage", type="integer", nullable=true)
     */
    private $passPercentage;

    /**
     * @var int|null
     *
     * @ORM\Column(name="question_selection_type", type="integer", nullable=true)
     */
    private $questionSelectionType;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="hide_question_title", type="boolean", nullable=true)
     */
    private $hideQuestionTitle;

    /**
     * @return int
     */
    public function getIid(): int
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid($iid)
    {
        $this->iid = $iid;
    }

    /**
     * @return int
     */
    public function getCId(): int
    {
        return $this->cId;
    }

    /**
     * @param int $cId
     */
    public function setCId(int $cId): void
    {
        $this->cId = $cId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getSound(): ?string
    {
        return $this->sound;
    }

    /**
     * @param null|string $sound
     */
    public function setSound(?string $sound): void
    {
        $this->sound = $sound;
    }

    /**
     * @return bool
     */
    public function isType(): bool
    {
        return $this->type;
    }

    /**
     * @param bool $type
     */
    public function setType(bool $type): void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getRandom(): int
    {
        return $this->random;
    }

    /**
     * @param int $random
     */
    public function setRandom(int $random): void
    {
        $this->random = $random;
    }

    /**
     * @return bool
     */
    public function isRandomAnswers(): bool
    {
        return $this->randomAnswers;
    }

    /**
     * @param bool $randomAnswers
     */
    public function setRandomAnswers(bool $randomAnswers): void
    {
        $this->randomAnswers = $randomAnswers;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getResultsDisabled(): int
    {
        return $this->resultsDisabled;
    }

    /**
     * @param int $resultsDisabled
     */
    public function setResultsDisabled(int $resultsDisabled): void
    {
        $this->resultsDisabled = $resultsDisabled;
    }

    /**
     * @return null|string
     */
    public function getAccessCondition(): ?string
    {
        return $this->accessCondition;
    }

    /**
     * @param null|string $accessCondition
     */
    public function setAccessCondition(?string $accessCondition): void
    {
        $this->accessCondition = $accessCondition;
    }

    /**
     * @return int
     */
    public function getMaxAttempt(): int
    {
        return $this->maxAttempt;
    }

    /**
     * @param int $maxAttempt
     */
    public function setMaxAttempt(int $maxAttempt): void
    {
        $this->maxAttempt = $maxAttempt;
    }

    /**
     * @return \DateTime|null
     */
    public function getStartTime(): ?\DateTime
    {
        return $this->startTime;
    }

    /**
     * @param \DateTime|null $startTime
     */
    public function setStartTime(?\DateTime $startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return \DateTime|null
     */
    public function getEndTime(): ?\DateTime
    {
        return $this->endTime;
    }

    /**
     * @param \DateTime|null $endTime
     */
    public function setEndTime(?\DateTime $endTime): void
    {
        $this->endTime = $endTime;
    }

    /**
     * @return int
     */
    public function getFeedbackType(): int
    {
        return $this->feedbackType;
    }

    /**
     * @param int $feedbackType
     */
    public function setFeedbackType(int $feedbackType): void
    {
        $this->feedbackType = $feedbackType;
    }

    /**
     * @return int
     */
    public function getExpiredTime(): int
    {
        return $this->expiredTime;
    }

    /**
     * @param int $expiredTime
     */
    public function setExpiredTime(int $expiredTime): void
    {
        $this->expiredTime = $expiredTime;
    }

    /**
     * @return int|null
     */
    public function getSessionId(): ?int
    {
        return $this->sessionId;
    }

    /**
     * @param int|null $sessionId
     */
    public function setSessionId(?int $sessionId): void
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return int
     */
    public function getPropagateNeg(): int
    {
        return $this->propagateNeg;
    }

    /**
     * @param int $propagateNeg
     */
    public function setPropagateNeg(int $propagateNeg): void
    {
        $this->propagateNeg = $propagateNeg;
    }

    /**
     * @return bool
     */
    public function isSaveCorrectAnswers(): bool
    {
        return $this->saveCorrectAnswers;
    }

    /**
     * @param bool $saveCorrectAnswers
     */
    public function setSaveCorrectAnswers(bool $saveCorrectAnswers): void
    {
        $this->saveCorrectAnswers = $saveCorrectAnswers;
    }

    /**
     * @return int
     */
    public function getReviewAnswers(): int
    {
        return $this->reviewAnswers;
    }

    /**
     * @param int $reviewAnswers
     */
    public function setReviewAnswers(int $reviewAnswers): void
    {
        $this->reviewAnswers = $reviewAnswers;
    }

    /**
     * @return int
     */
    public function getRandomByCategory(): int
    {
        return $this->randomByCategory;
    }

    /**
     * @param int $randomByCategory
     */
    public function setRandomByCategory(int $randomByCategory): void
    {
        $this->randomByCategory = $randomByCategory;
    }

    /**
     * @return null|string
     */
    public function getTextWhenFinished(): ?string
    {
        return $this->textWhenFinished;
    }

    /**
     * @param null|string $textWhenFinished
     */
    public function setTextWhenFinished(?string $textWhenFinished): void
    {
        $this->textWhenFinished = $textWhenFinished;
    }

    /**
     * @return int
     */
    public function getDisplayCategoryName(): int
    {
        return $this->displayCategoryName;
    }

    /**
     * @param int $displayCategoryName
     */
    public function setDisplayCategoryName(int $displayCategoryName): void
    {
        $this->displayCategoryName = $displayCategoryName;
    }

    /**
     * @return int|null
     */
    public function getPassPercentage(): ?int
    {
        return $this->passPercentage;
    }

    /**
     * @param int|null $passPercentage
     */
    public function setPassPercentage(?int $passPercentage): void
    {
        $this->passPercentage = $passPercentage;
    }

    /**
     * @return int|null
     */
    public function getQuestionSelectionType(): ?int
    {
        return $this->questionSelectionType;
    }

    /**
     * @param int|null $questionSelectionType
     */
    public function setQuestionSelectionType(?int $questionSelectionType): void
    {
        $this->questionSelectionType = $questionSelectionType;
    }

    /**
     * @return bool|null
     */
    public function getHideQuestionTitle(): ?bool
    {
        return $this->hideQuestionTitle;
    }

    /**
     * @param bool|null $hideQuestionTitle
     */
    public function setHideQuestionTitle(?bool $hideQuestionTitle): void
    {
        $this->hideQuestionTitle = $hideQuestionTitle;
    }


}

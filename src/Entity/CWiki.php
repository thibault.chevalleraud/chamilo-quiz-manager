<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CWiki
 *
 * @ORM\Table(name="c_wiki", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="reflink", columns={"reflink"}), @ORM\Index(name="group_id", columns={"group_id"}), @ORM\Index(name="page_id", columns={"page_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CWiki
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="page_id", type="integer", nullable=true)
     */
    private $pageId;

    /**
     * @var string
     *
     * @ORM\Column(name="reflink", type="string", length=255, nullable=false)
     */
    private $reflink;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=false)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dtime", type="datetime", nullable=true)
     */
    private $dtime;

    /**
     * @var int
     *
     * @ORM\Column(name="addlock", type="integer", nullable=false)
     */
    private $addlock;

    /**
     * @var int
     *
     * @ORM\Column(name="editlock", type="integer", nullable=false)
     */
    private $editlock;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility", type="integer", nullable=false)
     */
    private $visibility;

    /**
     * @var int
     *
     * @ORM\Column(name="addlock_disc", type="integer", nullable=false)
     */
    private $addlockDisc;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility_disc", type="integer", nullable=false)
     */
    private $visibilityDisc;

    /**
     * @var int
     *
     * @ORM\Column(name="ratinglock_disc", type="integer", nullable=false)
     */
    private $ratinglockDisc;

    /**
     * @var int
     *
     * @ORM\Column(name="assignment", type="integer", nullable=false)
     */
    private $assignment;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=false)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="progress", type="text", length=0, nullable=false)
     */
    private $progress;

    /**
     * @var int|null
     *
     * @ORM\Column(name="score", type="integer", nullable=true)
     */
    private $score;

    /**
     * @var int|null
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     */
    private $version;

    /**
     * @var int
     *
     * @ORM\Column(name="is_editing", type="integer", nullable=false)
     */
    private $isEditing;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time_edit", type="datetime", nullable=true)
     */
    private $timeEdit;

    /**
     * @var int|null
     *
     * @ORM\Column(name="hits", type="integer", nullable=true)
     */
    private $hits;

    /**
     * @var string
     *
     * @ORM\Column(name="linksto", type="text", length=0, nullable=false)
     */
    private $linksto;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="text", length=0, nullable=false)
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(name="user_ip", type="string", length=39, nullable=false)
     */
    private $userIp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;


}

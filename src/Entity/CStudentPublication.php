<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CStudentPublication
 *
 * @ORM\Table(name="c_student_publication", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session_id", columns={"session_id"}), @ORM\Index(name="idx_csp_u", columns={"user_id"})})
 * @ORM\Entity
 */
class CStudentPublication
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_correction", type="string", length=255, nullable=true)
     */
    private $urlCorrection;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title_correction", type="string", length=255, nullable=true)
     */
    private $titleCorrection;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="accepted", type="boolean", nullable=true)
     */
    private $accepted;

    /**
     * @var int
     *
     * @ORM\Column(name="post_group_id", type="integer", nullable=false)
     */
    private $postGroupId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sent_date", type="datetime", nullable=true)
     */
    private $sentDate;

    /**
     * @var string
     *
     * @ORM\Column(name="filetype", type="string", length=10, nullable=false)
     */
    private $filetype;

    /**
     * @var int
     *
     * @ORM\Column(name="has_properties", type="integer", nullable=false)
     */
    private $hasProperties;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="view_properties", type="boolean", nullable=true)
     */
    private $viewProperties;

    /**
     * @var float
     *
     * @ORM\Column(name="qualification", type="float", precision=10, scale=0, nullable=false)
     */
    private $qualification;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_of_qualification", type="datetime", nullable=true)
     */
    private $dateOfQualification;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var int
     *
     * @ORM\Column(name="qualificator_id", type="integer", nullable=false)
     */
    private $qualificatorId;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", precision=10, scale=0, nullable=false)
     */
    private $weight;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="allow_text_assignment", type="integer", nullable=false)
     */
    private $allowTextAssignment;

    /**
     * @var int
     *
     * @ORM\Column(name="contains_file", type="integer", nullable=false)
     */
    private $containsFile;

    /**
     * @var int
     *
     * @ORM\Column(name="document_id", type="integer", nullable=false)
     */
    private $documentId;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     * })
     */
    private $session;


}

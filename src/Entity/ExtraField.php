<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraField
 *
 * @ORM\Table(name="extra_field")
 * @ORM\Entity
 */
class ExtraField
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="extra_field_type", type="integer", nullable=false)
     */
    private $extraFieldType;

    /**
     * @var int
     *
     * @ORM\Column(name="field_type", type="integer", nullable=false)
     */
    private $fieldType;

    /**
     * @var string
     *
     * @ORM\Column(name="variable", type="string", length=255, nullable=false)
     */
    private $variable;

    /**
     * @var string|null
     *
     * @ORM\Column(name="display_text", type="string", length=255, nullable=true)
     */
    private $displayText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="default_value", type="text", length=0, nullable=true)
     */
    private $defaultValue;

    /**
     * @var int|null
     *
     * @ORM\Column(name="field_order", type="integer", nullable=true)
     */
    private $fieldOrder;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visible_to_self", type="boolean", nullable=true)
     */
    private $visibleToSelf;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visible_to_others", type="boolean", nullable=true)
     */
    private $visibleToOthers;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="changeable", type="boolean", nullable=true)
     */
    private $changeable;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="filter", type="boolean", nullable=true)
     */
    private $filter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GradebookCategory
 *
 * @ORM\Table(name="gradebook_category")
 * @ORM\Entity
 */
class GradebookCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=0, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="course_code", type="string", length=40, nullable=true)
     */
    private $courseCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", precision=10, scale=0, nullable=false)
     */
    private $weight;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible", type="boolean", nullable=false)
     */
    private $visible;

    /**
     * @var int|null
     *
     * @ORM\Column(name="certif_min_score", type="integer", nullable=true)
     */
    private $certifMinScore;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="document_id", type="integer", nullable=true)
     */
    private $documentId;

    /**
     * @var int
     *
     * @ORM\Column(name="locked", type="integer", nullable=false)
     */
    private $locked;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="default_lowest_eval_exclude", type="boolean", nullable=true)
     */
    private $defaultLowestEvalExclude;

    /**
     * @var bool
     *
     * @ORM\Column(name="generate_certificates", type="boolean", nullable=false)
     */
    private $generateCertificates;

    /**
     * @var int|null
     *
     * @ORM\Column(name="grade_model_id", type="integer", nullable=true)
     */
    private $gradeModelId;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_requirement", type="boolean", nullable=false)
     */
    private $isRequirement = '0';


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsergroupRelUser
 *
 * @ORM\Table(name="usergroup_rel_user", indexes={@ORM\Index(name="IDX_739515A9A76ED395", columns={"user_id"}), @ORM\Index(name="IDX_739515A9D2112630", columns={"usergroup_id"})})
 * @ORM\Entity
 */
class UsergroupRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="relation_type", type="integer", nullable=false)
     */
    private $relationType;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Usergroup
     *
     * @ORM\ManyToOne(targetEntity="Usergroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usergroup_id", referencedColumnName="id")
     * })
     */
    private $usergroup;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CGroupRelTutor
 *
 * @ORM\Table(name="c_group_rel_tutor", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CGroupRelTutor
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;


}

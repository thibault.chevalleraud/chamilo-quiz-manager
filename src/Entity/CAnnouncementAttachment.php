<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CAnnouncementAttachment
 *
 * @ORM\Table(name="c_announcement_attachment", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CAnnouncementAttachment
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     */
    private $size;

    /**
     * @var int
     *
     * @ORM\Column(name="announcement_id", type="integer", nullable=false)
     */
    private $announcementId;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;


}

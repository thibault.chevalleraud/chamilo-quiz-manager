<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CUserinfoContent
 *
 * @ORM\Table(name="c_userinfo_content", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class CUserinfoContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="definition_id", type="integer", nullable=false)
     */
    private $definitionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="editor_ip", type="string", length=39, nullable=true)
     */
    private $editorIp;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="edition_time", type="datetime", nullable=true)
     */
    private $editionTime;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=false)
     */
    private $content;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CRole
 *
 * @ORM\Table(name="c_role", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(name="role_name", type="string", length=250, nullable=false)
     */
    private $roleName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="role_comment", type="text", length=0, nullable=true)
     */
    private $roleComment;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="default_role", type="boolean", nullable=true)
     */
    private $defaultRole;

    /**
     * @var int
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceRuleCondition
 *
 * @ORM\Table(name="sequence_rule_condition", indexes={@ORM\Index(name="IDX_F948EE6A4044CA89", columns={"sequence_rule_id"}), @ORM\Index(name="IDX_F948EE6A8C0A7083", columns={"sequence_condition_id"})})
 * @ORM\Entity
 */
class SequenceRuleCondition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \SequenceRule
     *
     * @ORM\ManyToOne(targetEntity="SequenceRule")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_rule_id", referencedColumnName="id")
     * })
     */
    private $sequenceRule;

    /**
     * @var \SequenceCondition
     *
     * @ORM\ManyToOne(targetEntity="SequenceCondition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_condition_id", referencedColumnName="id")
     * })
     */
    private $sequenceCondition;


}

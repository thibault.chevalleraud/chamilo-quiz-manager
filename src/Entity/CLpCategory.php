<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CLpCategory
 *
 * @ORM\Table(name="c_lp_category", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CLpCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;


}

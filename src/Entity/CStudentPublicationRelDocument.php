<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CStudentPublicationRelDocument
 *
 * @ORM\Table(name="c_student_publication_rel_document", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="work", columns={"work_id"}), @ORM\Index(name="document", columns={"document_id"})})
 * @ORM\Entity
 */
class CStudentPublicationRelDocument
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="work_id", type="integer", nullable=false)
     */
    private $workId;

    /**
     * @var int
     *
     * @ORM\Column(name="document_id", type="integer", nullable=false)
     */
    private $documentId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CSurveyInvitation
 *
 * @ORM\Table(name="c_survey_invitation", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CSurveyInvitation
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_invitation_id", type="integer", nullable=false)
     */
    private $surveyInvitationId;

    /**
     * @var string
     *
     * @ORM\Column(name="survey_code", type="string", length=20, nullable=false)
     */
    private $surveyCode;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=250, nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="invitation_code", type="string", length=250, nullable=false)
     */
    private $invitationCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invitation_date", type="datetime", nullable=false)
     */
    private $invitationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reminder_date", type="datetime", nullable=false)
     */
    private $reminderDate;

    /**
     * @var int
     *
     * @ORM\Column(name="answered", type="integer", nullable=false)
     */
    private $answered;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;


}

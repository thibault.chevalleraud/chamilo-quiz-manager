<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CBlogComment
 *
 * @ORM\Table(name="c_blog_comment", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CBlogComment
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="comment_id", type="integer", nullable=false)
     */
    private $commentId;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=250, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=false)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="author_id", type="integer", nullable=false)
     */
    private $authorId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_id", type="integer", nullable=false)
     */
    private $blogId;

    /**
     * @var int
     *
     * @ORM\Column(name="post_id", type="integer", nullable=false)
     */
    private $postId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="task_id", type="integer", nullable=true)
     */
    private $taskId;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_comment_id", type="integer", nullable=false)
     */
    private $parentCommentId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackStoredValues
 *
 * @ORM\Table(name="track_stored_values", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_2", columns={"user_id", "sco_id", "course_id", "sv_key"})}, indexes={@ORM\Index(name="user_sco_course_sv", columns={"user_id", "sco_id", "course_id", "sv_key"})})
 * @ORM\Entity
 */
class TrackStoredValues
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="sco_id", type="integer", nullable=false)
     */
    private $scoId;

    /**
     * @var string
     *
     * @ORM\Column(name="course_id", type="string", length=40, nullable=false)
     */
    private $courseId;

    /**
     * @var string
     *
     * @ORM\Column(name="sv_key", type="string", length=64, nullable=false)
     */
    private $svKey;

    /**
     * @var string
     *
     * @ORM\Column(name="sv_value", type="text", length=0, nullable=false)
     */
    private $svValue;


}

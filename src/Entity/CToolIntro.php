<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CToolIntro
 *
 * @ORM\Table(name="c_tool_intro", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CToolIntro
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255, nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intro_text", type="text", length=0, nullable=false)
     */
    private $introText;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;


}

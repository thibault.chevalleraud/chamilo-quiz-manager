<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CStudentPublicationAssignment
 *
 * @ORM\Table(name="c_student_publication_assignment", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CStudentPublicationAssignment
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expires_on", type="datetime", nullable=true)
     */
    private $expiresOn;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ends_on", type="datetime", nullable=true)
     */
    private $endsOn;

    /**
     * @var int
     *
     * @ORM\Column(name="add_to_calendar", type="integer", nullable=false)
     */
    private $addToCalendar;

    /**
     * @var bool
     *
     * @ORM\Column(name="enable_qualification", type="boolean", nullable=false)
     */
    private $enableQualification;

    /**
     * @var int
     *
     * @ORM\Column(name="publication_id", type="integer", nullable=false)
     */
    private $publicationId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CCalendarEventRepeat
 *
 * @ORM\Table(name="c_calendar_event_repeat", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CCalendarEventRepeat
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="cal_id", type="integer", nullable=false)
     */
    private $calId;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cal_type", type="string", length=20, nullable=true)
     */
    private $calType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cal_end", type="integer", nullable=true)
     */
    private $calEnd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cal_frequency", type="integer", nullable=true)
     */
    private $calFrequency;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cal_days", type="string", length=7, nullable=true)
     */
    private $calDays;


}

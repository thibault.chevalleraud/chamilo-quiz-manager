<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HookCall
 *
 * @ORM\Table(name="hook_call")
 * @ORM\Entity
 */
class HookCall
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="hook_event_id", type="integer", nullable=false)
     */
    private $hookEventId;

    /**
     * @var int
     *
     * @ORM\Column(name="hook_observer_id", type="integer", nullable=false)
     */
    private $hookObserverId;

    /**
     * @var bool
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="hook_order", type="integer", nullable=false)
     */
    private $hookOrder;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;


}

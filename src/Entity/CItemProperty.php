<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CItemProperty
 *
 * @ORM\Table(name="c_item_property", indexes={@ORM\Index(name="IDX_1D84C18191D79BD3", columns={"c_id"}), @ORM\Index(name="IDX_1D84C181330D47E9", columns={"to_group_id"}), @ORM\Index(name="IDX_1D84C18129F6EE60", columns={"to_user_id"}), @ORM\Index(name="IDX_1D84C1819C859CC3", columns={"insert_user_id"}), @ORM\Index(name="IDX_1D84C181613FECDF", columns={"session_id"}), @ORM\Index(name="idx_item_property_toolref", columns={"tool", "ref"})})
 * @ORM\Entity
 */
class CItemProperty
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tool", type="string", length=100, nullable=false)
     */
    private $tool;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_date", type="datetime", nullable=false)
     */
    private $insertDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastedit_date", type="datetime", nullable=false)
     */
    private $lasteditDate;

    /**
     * @var int
     *
     * @ORM\Column(name="ref", type="integer", nullable=false)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\Column(name="lastedit_type", type="string", length=100, nullable=false)
     */
    private $lasteditType;

    /**
     * @var int
     *
     * @ORM\Column(name="lastedit_user_id", type="integer", nullable=false)
     */
    private $lasteditUserId;

    /**
     * @var int
     *
     * @ORM\Column(name="visibility", type="integer", nullable=false)
     */
    private $visibility;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="start_visible", type="datetime", nullable=true)
     */
    private $startVisible;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_visible", type="datetime", nullable=true)
     */
    private $endVisible;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="to_user_id", referencedColumnName="id")
     * })
     */
    private $toUser;

    /**
     * @var \CGroupInfo
     *
     * @ORM\ManyToOne(targetEntity="CGroupInfo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="to_group_id", referencedColumnName="iid")
     * })
     */
    private $toGroup;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     * })
     */
    private $session;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="c_id", referencedColumnName="id")
     * })
     */
    private $c;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="insert_user_id", referencedColumnName="id")
     * })
     */
    private $insertUser;


}

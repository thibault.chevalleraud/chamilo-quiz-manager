<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CAttendanceCalendar
 *
 * @ORM\Table(name="c_attendance_calendar", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="attendance_id", columns={"attendance_id"}), @ORM\Index(name="done_attendance", columns={"done_attendance"})})
 * @ORM\Entity
 */
class CAttendanceCalendar
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="attendance_id", type="integer", nullable=false)
     */
    private $attendanceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_time", type="datetime", nullable=false)
     */
    private $dateTime;

    /**
     * @var bool
     *
     * @ORM\Column(name="done_attendance", type="boolean", nullable=false)
     */
    private $doneAttendance;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CSurveyQuestionOption
 *
 * @ORM\Table(name="c_survey_question_option", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CSurveyQuestionOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="question_option_id", type="integer", nullable=false)
     */
    private $questionOptionId;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=false)
     */
    private $questionId;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_id", type="integer", nullable=false)
     */
    private $surveyId;

    /**
     * @var string
     *
     * @ORM\Column(name="option_text", type="text", length=0, nullable=false)
     */
    private $optionText;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     */
    private $sort;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CDropboxPost
 *
 * @ORM\Table(name="c_dropbox_post", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="dest_user", columns={"dest_user_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CDropboxPost
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="feedback_date", type="datetime", nullable=false)
     */
    private $feedbackDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="feedback", type="text", length=0, nullable=true)
     */
    private $feedback;

    /**
     * @var int
     *
     * @ORM\Column(name="cat_id", type="integer", nullable=false)
     */
    private $catId;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="file_id", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var int
     *
     * @ORM\Column(name="dest_user_id", type="integer", nullable=false)
     */
    private $destUserId;


}

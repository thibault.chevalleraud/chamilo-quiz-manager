<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionRelUser
 *
 * @ORM\Table(name="session_rel_user", indexes={@ORM\Index(name="IDX_B0D7D4C0613FECDF", columns={"session_id"}), @ORM\Index(name="IDX_B0D7D4C0A76ED395", columns={"user_id"}), @ORM\Index(name="idx_session_rel_user_id_user_moved", columns={"user_id", "moved_to"})})
 * @ORM\Entity
 */
class SessionRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="relation_type", type="integer", nullable=false)
     */
    private $relationType;

    /**
     * @var int|null
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var int|null
     *
     * @ORM\Column(name="moved_to", type="integer", nullable=true)
     */
    private $movedTo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="moved_status", type="integer", nullable=true)
     */
    private $movedStatus;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="moved_at", type="datetime", nullable=true)
     */
    private $movedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registered_at", type="datetime", nullable=false)
     */
    private $registeredAt;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     * })
     */
    private $session;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

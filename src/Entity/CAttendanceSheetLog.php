<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CAttendanceSheetLog
 *
 * @ORM\Table(name="c_attendance_sheet_log", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CAttendanceSheetLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="attendance_id", type="integer", nullable=false)
     */
    private $attendanceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastedit_date", type="datetime", nullable=false)
     */
    private $lasteditDate;

    /**
     * @var string
     *
     * @ORM\Column(name="lastedit_type", type="string", length=200, nullable=false)
     */
    private $lasteditType;

    /**
     * @var int
     *
     * @ORM\Column(name="lastedit_user_id", type="integer", nullable=false)
     */
    private $lasteditUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="calendar_date_value", type="datetime", nullable=true)
     */
    private $calendarDateValue;


}

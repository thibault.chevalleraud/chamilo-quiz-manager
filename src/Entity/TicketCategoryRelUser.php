<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketCategoryRelUser
 *
 * @ORM\Table(name="ticket_category_rel_user", indexes={@ORM\Index(name="IDX_5B8A98712469DE2", columns={"category_id"}), @ORM\Index(name="IDX_5B8A987A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class TicketCategoryRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TicketCategory
     *
     * @ORM\ManyToOne(targetEntity="TicketCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

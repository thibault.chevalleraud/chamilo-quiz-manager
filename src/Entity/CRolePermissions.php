<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CRolePermissions
 *
 * @ORM\Table(name="c_role_permissions", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="role", columns={"role_id"})})
 * @ORM\Entity
 */
class CRolePermissions
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="default_perm", type="boolean", nullable=false)
     */
    private $defaultPerm;

    /**
     * @var int
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * @var string
     *
     * @ORM\Column(name="tool", type="string", length=250, nullable=false)
     */
    private $tool;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=50, nullable=false)
     */
    private $action;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CLpView
 *
 * @ORM\Table(name="c_lp_view", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="lp_id", columns={"lp_id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class CLpView
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="lp_id", type="integer", nullable=false)
     */
    private $lpId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="view_count", type="integer", nullable=false)
     */
    private $viewCount;

    /**
     * @var int
     *
     * @ORM\Column(name="last_item", type="integer", nullable=false)
     */
    private $lastItem;

    /**
     * @var int|null
     *
     * @ORM\Column(name="progress", type="integer", nullable=true)
     */
    private $progress;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CBlogRelUser
 *
 * @ORM\Table(name="c_blog_rel_user", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CBlogRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="blog_id", type="integer", nullable=false)
     */
    private $blogId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;


}

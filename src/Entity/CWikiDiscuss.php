<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CWikiDiscuss
 *
 * @ORM\Table(name="c_wiki_discuss", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CWikiDiscuss
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="publication_id", type="integer", nullable=false)
     */
    private $publicationId;

    /**
     * @var int
     *
     * @ORM\Column(name="userc_id", type="integer", nullable=false)
     */
    private $usercId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=false)
     */
    private $comment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="p_score", type="string", length=255, nullable=true)
     */
    private $pScore;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dtime", type="datetime", nullable=false)
     */
    private $dtime;


}

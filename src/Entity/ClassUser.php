<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClassUser
 *
 * @ORM\Table(name="class_user")
 * @ORM\Entity
 */
class ClassUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="class_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $classId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CForumForum
 *
 * @ORM\Table(name="c_forum_forum", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CForumForum
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="forum_id", type="integer", nullable=false)
     */
    private $forumId;

    /**
     * @var string
     *
     * @ORM\Column(name="forum_title", type="string", length=255, nullable=false)
     */
    private $forumTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="forum_comment", type="text", length=0, nullable=true)
     */
    private $forumComment;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_threads", type="integer", nullable=true)
     */
    private $forumThreads;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_posts", type="integer", nullable=true)
     */
    private $forumPosts;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_last_post", type="integer", nullable=true)
     */
    private $forumLastPost;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_category", type="integer", nullable=true)
     */
    private $forumCategory;

    /**
     * @var int|null
     *
     * @ORM\Column(name="allow_anonymous", type="integer", nullable=true)
     */
    private $allowAnonymous;

    /**
     * @var int|null
     *
     * @ORM\Column(name="allow_edit", type="integer", nullable=true)
     */
    private $allowEdit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="approval_direct_post", type="string", length=20, nullable=true)
     */
    private $approvalDirectPost;

    /**
     * @var int|null
     *
     * @ORM\Column(name="allow_attachments", type="integer", nullable=true)
     */
    private $allowAttachments;

    /**
     * @var int|null
     *
     * @ORM\Column(name="allow_new_threads", type="integer", nullable=true)
     */
    private $allowNewThreads;

    /**
     * @var string|null
     *
     * @ORM\Column(name="default_view", type="string", length=20, nullable=true)
     */
    private $defaultView;

    /**
     * @var string|null
     *
     * @ORM\Column(name="forum_of_group", type="string", length=20, nullable=true)
     */
    private $forumOfGroup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="forum_group_public_private", type="string", length=20, nullable=true)
     */
    private $forumGroupPublicPrivate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_order", type="integer", nullable=true)
     */
    private $forumOrder;

    /**
     * @var int
     *
     * @ORM\Column(name="locked", type="integer", nullable=false)
     */
    private $locked;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="forum_image", type="string", length=255, nullable=false)
     */
    private $forumImage;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true)
     */
    private $endTime;

    /**
     * @var int
     *
     * @ORM\Column(name="lp_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $lpId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="moderated", type="boolean", nullable=true)
     */
    private $moderated;


}

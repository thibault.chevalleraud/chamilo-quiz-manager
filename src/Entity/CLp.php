<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CLp
 *
 * @ORM\Table(name="c_lp", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="session", columns={"session_id"})})
 * @ORM\Entity
 */
class CLp
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="lp_type", type="integer", nullable=false)
     */
    private $lpType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ref", type="text", length=0, nullable=true)
     */
    private $ref;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text", length=0, nullable=false)
     */
    private $path;

    /**
     * @var bool
     *
     * @ORM\Column(name="force_commit", type="boolean", nullable=false)
     */
    private $forceCommit;

    /**
     * @var string
     *
     * @ORM\Column(name="default_view_mod", type="string", length=32, nullable=false, options={"default"="embedded"})
     */
    private $defaultViewMod = 'embedded';

    /**
     * @var string
     *
     * @ORM\Column(name="default_encoding", type="string", length=32, nullable=false, options={"default"="UTF-8"})
     */
    private $defaultEncoding = 'UTF-8';

    /**
     * @var int
     *
     * @ORM\Column(name="display_order", type="integer", nullable=false)
     */
    private $displayOrder = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="content_maker", type="text", length=0, nullable=false)
     */
    private $contentMaker;

    /**
     * @var string
     *
     * @ORM\Column(name="content_local", type="string", length=32, nullable=false, options={"default"="local"})
     */
    private $contentLocal = 'local';

    /**
     * @var string
     *
     * @ORM\Column(name="content_license", type="text", length=0, nullable=false)
     */
    private $contentLicense;

    /**
     * @var bool
     *
     * @ORM\Column(name="prevent_reinit", type="boolean", nullable=false, options={"default"="1"})
     */
    private $preventReinit = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="js_lib", type="text", length=0, nullable=false)
     */
    private $jsLib;

    /**
     * @var bool
     *
     * @ORM\Column(name="debug", type="boolean", nullable=false)
     */
    private $debug;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255, nullable=false)
     */
    private $theme;

    /**
     * @var string
     *
     * @ORM\Column(name="preview_image", type="string", length=255, nullable=false)
     */
    private $previewImage;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=false)
     */
    private $author;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="prerequisite", type="integer", nullable=false)
     */
    private $prerequisite;

    /**
     * @var bool
     *
     * @ORM\Column(name="hide_toc_frame", type="boolean", nullable=false)
     */
    private $hideTocFrame;

    /**
     * @var bool
     *
     * @ORM\Column(name="seriousgame_mode", type="boolean", nullable=false)
     */
    private $seriousgameMode;

    /**
     * @var int
     *
     * @ORM\Column(name="use_max_score", type="integer", nullable=false, options={"default"="1"})
     */
    private $useMaxScore = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="autolaunch", type="integer", nullable=false)
     */
    private $autolaunch;

    /**
     * @var int
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     */
    private $categoryId;

    /**
     * @var int
     *
     * @ORM\Column(name="max_attempts", type="integer", nullable=false)
     */
    private $maxAttempts;

    /**
     * @var int
     *
     * @ORM\Column(name="subscribe_users", type="integer", nullable=false)
     */
    private $subscribeUsers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime", nullable=false)
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_on", type="datetime", nullable=false)
     */
    private $modifiedOn;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="publicated_on", type="datetime", nullable=true)
     */
    private $publicatedOn;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expired_on", type="datetime", nullable=true)
     */
    private $expiredOn;

    /**
     * @var int
     *
     * @ORM\Column(name="accumulate_scorm_time", type="integer", nullable=false, options={"default"="1"})
     */
    private $accumulateScormTime = '1';


}

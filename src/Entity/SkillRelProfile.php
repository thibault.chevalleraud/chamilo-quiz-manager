<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkillRelProfile
 *
 * @ORM\Table(name="skill_rel_profile")
 * @ORM\Entity
 */
class SkillRelProfile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="skill_id", type="integer", nullable=false)
     */
    private $skillId;

    /**
     * @var int
     *
     * @ORM\Column(name="profile_id", type="integer", nullable=false)
     */
    private $profileId;


}

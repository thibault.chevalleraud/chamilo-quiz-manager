<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkillProfile
 *
 * @ORM\Table(name="skill_profile")
 * @ORM\Entity
 */
class SkillProfile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraFieldSavedSearch
 *
 * @ORM\Table(name="extra_field_saved_search", indexes={@ORM\Index(name="IDX_16ABE32A443707B0", columns={"field_id"}), @ORM\Index(name="IDX_16ABE32AA76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class ExtraFieldSavedSearch
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var array|null
     *
     * @ORM\Column(name="value", type="array", length=0, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \ExtraField
     *
     * @ORM\ManyToOne(targetEntity="ExtraField")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


}

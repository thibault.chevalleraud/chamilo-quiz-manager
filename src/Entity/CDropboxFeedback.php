<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CDropboxFeedback
 *
 * @ORM\Table(name="c_dropbox_feedback", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="file_id", columns={"file_id"}), @ORM\Index(name="author_user_id", columns={"author_user_id"})})
 * @ORM\Entity
 */
class CDropboxFeedback
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="feedback_id", type="integer", nullable=false)
     */
    private $feedbackId;

    /**
     * @var int
     *
     * @ORM\Column(name="file_id", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var int
     *
     * @ORM\Column(name="author_user_id", type="integer", nullable=false)
     */
    private $authorUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="feedback", type="text", length=0, nullable=false)
     */
    private $feedback;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="feedback_date", type="datetime", nullable=false)
     */
    private $feedbackDate;


}

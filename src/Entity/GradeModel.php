<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GradeModel
 *
 * @ORM\Table(name="grade_model")
 * @ORM\Entity
 */
class GradeModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="default_lowest_eval_exclude", type="boolean", nullable=true)
     */
    private $defaultLowestEvalExclude;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="default_external_eval", type="boolean", nullable=true)
     */
    private $defaultExternalEval;

    /**
     * @var string|null
     *
     * @ORM\Column(name="default_external_eval_prefix", type="string", length=140, nullable=true)
     */
    private $defaultExternalEvalPrefix;


}

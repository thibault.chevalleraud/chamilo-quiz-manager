<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CRoleGroup
 *
 * @ORM\Table(name="c_role_group", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="group", columns={"group_id"})})
 * @ORM\Entity
 */
class CRoleGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="role_id", type="integer", nullable=false)
     */
    private $roleId;

    /**
     * @var string
     *
     * @ORM\Column(name="scope", type="string", length=20, nullable=false)
     */
    private $scope;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;


}

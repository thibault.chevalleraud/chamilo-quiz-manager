<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketTicket
 *
 * @ORM\Table(name="ticket_ticket", indexes={@ORM\Index(name="IDX_EDE2C768166D1F9C", columns={"project_id"}), @ORM\Index(name="IDX_EDE2C76812469DE2", columns={"category_id"}), @ORM\Index(name="IDX_EDE2C768497B19F9", columns={"priority_id"}), @ORM\Index(name="IDX_EDE2C768591CC992", columns={"course_id"}), @ORM\Index(name="IDX_EDE2C768613FECDF", columns={"session_id"}), @ORM\Index(name="IDX_EDE2C7686BF700BD", columns={"status_id"})})
 * @ORM\Entity
 */
class TicketTicket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string|null
     *
     * @ORM\Column(name="message", type="text", length=0, nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_email", type="string", length=255, nullable=false)
     */
    private $personalEmail;

    /**
     * @var int|null
     *
     * @ORM\Column(name="assigned_last_user", type="integer", nullable=true)
     */
    private $assignedLastUser;

    /**
     * @var int
     *
     * @ORM\Column(name="total_messages", type="integer", nullable=false)
     */
    private $totalMessages;

    /**
     * @var string|null
     *
     * @ORM\Column(name="keyword", type="string", length=255, nullable=true)
     */
    private $keyword;

    /**
     * @var string|null
     *
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="sys_insert_user_id", type="integer", nullable=false)
     */
    private $sysInsertUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sys_insert_datetime", type="datetime", nullable=false)
     */
    private $sysInsertDatetime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sys_lastedit_user_id", type="integer", nullable=true)
     */
    private $sysLasteditUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sys_lastedit_datetime", type="datetime", nullable=true)
     */
    private $sysLasteditDatetime;

    /**
     * @var \TicketCategory
     *
     * @ORM\ManyToOne(targetEntity="TicketCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \TicketProject
     *
     * @ORM\ManyToOne(targetEntity="TicketProject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;

    /**
     * @var \TicketPriority
     *
     * @ORM\ManyToOne(targetEntity="TicketPriority")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="priority_id", referencedColumnName="id")
     * })
     */
    private $priority;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * })
     */
    private $course;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     * })
     */
    private $session;

    /**
     * @var \TicketStatus
     *
     * @ORM\ManyToOne(targetEntity="TicketStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     */
    private $status;


}

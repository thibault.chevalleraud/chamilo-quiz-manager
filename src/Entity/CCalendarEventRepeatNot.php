<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CCalendarEventRepeatNot
 *
 * @ORM\Table(name="c_calendar_event_repeat_not", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CCalendarEventRepeatNot
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="cal_id", type="integer", nullable=false)
     */
    private $calId;

    /**
     * @var int
     *
     * @ORM\Column(name="cal_date", type="integer", nullable=false)
     */
    private $calDate;


}

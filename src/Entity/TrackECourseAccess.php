<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackECourseAccess
 *
 * @ORM\Table(name="track_e_course_access", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="login_course_date", columns={"login_course_date"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class TrackECourseAccess
{
    /**
     * @var int
     *
     * @ORM\Column(name="course_access_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $courseAccessId;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="login_course_date", type="datetime", nullable=false)
     */
    private $loginCourseDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="logout_course_date", type="datetime", nullable=true)
     */
    private $logoutCourseDate;

    /**
     * @var int
     *
     * @ORM\Column(name="counter", type="integer", nullable=false)
     */
    private $counter;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var string
     *
     * @ORM\Column(name="user_ip", type="string", length=39, nullable=false)
     */
    private $userIp;


}

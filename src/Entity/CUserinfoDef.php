<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CUserinfoDef
 *
 * @ORM\Table(name="c_userinfo_def", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class CUserinfoDef
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=80, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var bool
     *
     * @ORM\Column(name="line_count", type="boolean", nullable=false)
     */
    private $lineCount;

    /**
     * @var bool
     *
     * @ORM\Column(name="rank", type="boolean", nullable=false)
     */
    private $rank;


}

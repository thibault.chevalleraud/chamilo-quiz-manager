<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Course
 *
 * @ORM\Table(name="course", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_169E6FB977153098", columns={"code"})}, indexes={@ORM\Index(name="IDX_169E6FB954177093", columns={"room_id"}), @ORM\Index(name="category_code", columns={"category_code"}), @ORM\Index(name="directory", columns={"directory"})})
 * @ORM\Entity
 */
class Course
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=250, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=40, nullable=false)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="directory", type="string", length=40, nullable=true)
     */
    private $directory;

    /**
     * @var string|null
     *
     * @ORM\Column(name="course_language", type="string", length=20, nullable=true)
     */
    private $courseLanguage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="category_code", type="string", length=40, nullable=true)
     */
    private $categoryCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="visibility", type="integer", nullable=true)
     */
    private $visibility;

    /**
     * @var int|null
     *
     * @ORM\Column(name="show_score", type="integer", nullable=true)
     */
    private $showScore;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tutor_name", type="string", length=200, nullable=true)
     */
    private $tutorName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="visual_code", type="string", length=40, nullable=true)
     */
    private $visualCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="department_name", type="string", length=30, nullable=true)
     */
    private $departmentName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="department_url", type="string", length=180, nullable=true)
     */
    private $departmentUrl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="disk_quota", type="bigint", nullable=true)
     */
    private $diskQuota;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_visit", type="datetime", nullable=true)
     */
    private $lastVisit;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_edit", type="datetime", nullable=true)
     */
    private $lastEdit;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expiration_date", type="datetime", nullable=true)
     */
    private $expirationDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="subscribe", type="boolean", nullable=true)
     */
    private $subscribe;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="unsubscribe", type="boolean", nullable=true)
     */
    private $unsubscribe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="registration_code", type="string", length=255, nullable=true)
     */
    private $registrationCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legal", type="text", length=0, nullable=true)
     */
    private $legal;

    /**
     * @var int|null
     *
     * @ORM\Column(name="activate_legal", type="integer", nullable=true)
     */
    private $activateLegal;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="add_teachers_to_sessions_courses", type="boolean", nullable=true)
     */
    private $addTeachersToSessionsCourses;

    /**
     * @var int|null
     *
     * @ORM\Column(name="course_type_id", type="integer", nullable=true)
     */
    private $courseTypeId;

    /**
     * @var \Room
     *
     * @ORM\ManyToOne(targetEntity="Room")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_id", referencedColumnName="id")
     * })
     */
    private $room;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return null|string
     */
    public function getDirectory(): ?string
    {
        return $this->directory;
    }

    /**
     * @param null|string $directory
     */
    public function setDirectory(?string $directory): void
    {
        $this->directory = $directory;
    }

    /**
     * @return null|string
     */
    public function getCourseLanguage(): ?string
    {
        return $this->courseLanguage;
    }

    /**
     * @param null|string $courseLanguage
     */
    public function setCourseLanguage(?string $courseLanguage): void
    {
        $this->courseLanguage = $courseLanguage;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return null|string
     */
    public function getCategoryCode(): ?string
    {
        return $this->categoryCode;
    }

    /**
     * @param null|string $categoryCode
     */
    public function setCategoryCode(?string $categoryCode): void
    {
        $this->categoryCode = $categoryCode;
    }

    /**
     * @return int|null
     */
    public function getVisibility(): ?int
    {
        return $this->visibility;
    }

    /**
     * @param int|null $visibility
     */
    public function setVisibility(?int $visibility): void
    {
        $this->visibility = $visibility;
    }

    /**
     * @return int|null
     */
    public function getShowScore(): ?int
    {
        return $this->showScore;
    }

    /**
     * @param int|null $showScore
     */
    public function setShowScore(?int $showScore): void
    {
        $this->showScore = $showScore;
    }

    /**
     * @return null|string
     */
    public function getTutorName(): ?string
    {
        return $this->tutorName;
    }

    /**
     * @param null|string $tutorName
     */
    public function setTutorName(?string $tutorName): void
    {
        $this->tutorName = $tutorName;
    }

    /**
     * @return null|string
     */
    public function getVisualCode(): ?string
    {
        return $this->visualCode;
    }

    /**
     * @param null|string $visualCode
     */
    public function setVisualCode(?string $visualCode): void
    {
        $this->visualCode = $visualCode;
    }

    /**
     * @return null|string
     */
    public function getDepartmentName(): ?string
    {
        return $this->departmentName;
    }

    /**
     * @param null|string $departmentName
     */
    public function setDepartmentName(?string $departmentName): void
    {
        $this->departmentName = $departmentName;
    }

    /**
     * @return null|string
     */
    public function getDepartmentUrl(): ?string
    {
        return $this->departmentUrl;
    }

    /**
     * @param null|string $departmentUrl
     */
    public function setDepartmentUrl(?string $departmentUrl): void
    {
        $this->departmentUrl = $departmentUrl;
    }

    /**
     * @return int|null
     */
    public function getDiskQuota(): ?int
    {
        return $this->diskQuota;
    }

    /**
     * @param int|null $diskQuota
     */
    public function setDiskQuota(?int $diskQuota): void
    {
        $this->diskQuota = $diskQuota;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastVisit(): ?\DateTime
    {
        return $this->lastVisit;
    }

    /**
     * @param \DateTime|null $lastVisit
     */
    public function setLastVisit(?\DateTime $lastVisit): void
    {
        $this->lastVisit = $lastVisit;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastEdit(): ?\DateTime
    {
        return $this->lastEdit;
    }

    /**
     * @param \DateTime|null $lastEdit
     */
    public function setLastEdit(?\DateTime $lastEdit): void
    {
        $this->lastEdit = $lastEdit;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreationDate(): ?\DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime|null $creationDate
     */
    public function setCreationDate(?\DateTime $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpirationDate(): ?\DateTime
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime|null $expirationDate
     */
    public function setExpirationDate(?\DateTime $expirationDate): void
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return bool|null
     */
    public function getSubscribe(): ?bool
    {
        return $this->subscribe;
    }

    /**
     * @param bool|null $subscribe
     */
    public function setSubscribe(?bool $subscribe): void
    {
        $this->subscribe = $subscribe;
    }

    /**
     * @return bool|null
     */
    public function getUnsubscribe(): ?bool
    {
        return $this->unsubscribe;
    }

    /**
     * @param bool|null $unsubscribe
     */
    public function setUnsubscribe(?bool $unsubscribe): void
    {
        $this->unsubscribe = $unsubscribe;
    }

    /**
     * @return null|string
     */
    public function getRegistrationCode(): ?string
    {
        return $this->registrationCode;
    }

    /**
     * @param null|string $registrationCode
     */
    public function setRegistrationCode(?string $registrationCode): void
    {
        $this->registrationCode = $registrationCode;
    }

    /**
     * @return null|string
     */
    public function getLegal(): ?string
    {
        return $this->legal;
    }

    /**
     * @param null|string $legal
     */
    public function setLegal(?string $legal): void
    {
        $this->legal = $legal;
    }


}

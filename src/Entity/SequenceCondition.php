<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceCondition
 *
 * @ORM\Table(name="sequence_condition")
 * @ORM\Entity
 */
class SequenceCondition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="mat_op", type="string", length=255, nullable=false)
     */
    private $matOp;

    /**
     * @var float
     *
     * @ORM\Column(name="param", type="float", precision=10, scale=0, nullable=false)
     */
    private $param;

    /**
     * @var int
     *
     * @ORM\Column(name="act_true", type="integer", nullable=false)
     */
    private $actTrue;

    /**
     * @var string
     *
     * @ORM\Column(name="act_false", type="string", length=255, nullable=false)
     */
    private $actFalse;


}

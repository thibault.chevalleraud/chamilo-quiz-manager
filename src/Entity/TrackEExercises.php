<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackEExercises
 *
 * @ORM\Table(name="track_e_exercises", indexes={@ORM\Index(name="idx_tee_user_id", columns={"exe_user_id"}), @ORM\Index(name="idx_tee_c_id", columns={"c_id"}), @ORM\Index(name="session_id", columns={"session_id"})})
 * @ORM\Entity
 */
class TrackEExercises
{
    /**
     * @var int
     *
     * @ORM\Column(name="exe_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $exeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="exe_user_id", type="integer", nullable=true)
     */
    private $exeUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="exe_date", type="datetime", nullable=false)
     */
    private $exeDate;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="exe_exo_id", type="integer", nullable=false)
     */
    private $exeExoId;

    /**
     * @var float
     *
     * @ORM\Column(name="exe_result", type="float", precision=10, scale=0, nullable=false)
     */
    private $exeResult;

    /**
     * @var float
     *
     * @ORM\Column(name="exe_weighting", type="float", precision=10, scale=0, nullable=false)
     */
    private $exeWeighting;

    /**
     * @var string
     *
     * @ORM\Column(name="user_ip", type="string", length=39, nullable=false)
     */
    private $userIp;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="data_tracking", type="text", length=0, nullable=false)
     */
    private $dataTracking;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     */
    private $startDate;

    /**
     * @var int
     *
     * @ORM\Column(name="steps_counter", type="smallint", nullable=false)
     */
    private $stepsCounter;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="smallint", nullable=false)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="orig_lp_id", type="integer", nullable=false)
     */
    private $origLpId;

    /**
     * @var int
     *
     * @ORM\Column(name="orig_lp_item_id", type="integer", nullable=false)
     */
    private $origLpItemId;

    /**
     * @var int
     *
     * @ORM\Column(name="exe_duration", type="integer", nullable=false)
     */
    private $exeDuration;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="expired_time_control", type="datetime", nullable=true)
     */
    private $expiredTimeControl;

    /**
     * @var int
     *
     * @ORM\Column(name="orig_lp_item_view_id", type="integer", nullable=false)
     */
    private $origLpItemViewId;

    /**
     * @var string
     *
     * @ORM\Column(name="questions_to_check", type="text", length=0, nullable=false)
     */
    private $questionsToCheck;


}

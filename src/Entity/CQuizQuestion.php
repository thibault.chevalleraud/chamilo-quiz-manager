<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuizQuestion
 *
 * @ORM\Table(name="c_quiz_question", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="position", columns={"position"})})
 * @ORM\Entity
 */
class CQuizQuestion
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="text", length=0, nullable=false)
     */
    private $question;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="ponderation", type="float", precision=10, scale=0, nullable=false)
     */
    private $ponderation = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="picture", type="string", length=50, nullable=true)
     */
    private $picture;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer", nullable=false)
     */
    private $level;

    /**
     * @var string|null
     *
     * @ORM\Column(name="extra", type="string", length=255, nullable=true)
     */
    private $extra;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question_code", type="string", length=10, nullable=true)
     */
    private $questionCode;

    /**
     * @return int
     */
    public function getIid(): int
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid(int $iid): void
    {
        $this->iid = $iid;
    }

    /**
     * @return int
     */
    public function getCId(): int
    {
        return $this->cId;
    }

    /**
     * @param int $cId
     */
    public function setCId(int $cId): void
    {
        $this->cId = $cId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPonderation(): float
    {
        return $this->ponderation;
    }

    /**
     * @param float $ponderation
     */
    public function setPonderation(float $ponderation): void
    {
        $this->ponderation = $ponderation;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return null|string
     */
    public function getPicture(): ?string
    {
        return $this->picture;
    }

    /**
     * @param null|string $picture
     */
    public function setPicture(?string $picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level): void
    {
        $this->level = $level;
    }

    /**
     * @return null|string
     */
    public function getExtra(): ?string
    {
        return $this->extra;
    }

    /**
     * @param null|string $extra
     */
    public function setExtra(?string $extra): void
    {
        $this->extra = $extra;
    }

    /**
     * @return null|string
     */
    public function getQuestionCode(): ?string
    {
        return $this->questionCode;
    }

    /**
     * @param null|string $questionCode
     */
    public function setQuestionCode(?string $questionCode): void
    {
        $this->questionCode = $questionCode;
    }


}

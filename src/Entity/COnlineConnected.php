<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * COnlineConnected
 *
 * @ORM\Table(name="c_online_connected", indexes={@ORM\Index(name="course", columns={"c_id"})})
 * @ORM\Entity
 */
class COnlineConnected
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_connection", type="datetime", nullable=false)
     */
    private $lastConnection;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;


}

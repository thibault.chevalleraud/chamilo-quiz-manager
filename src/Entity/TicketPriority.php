<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketPriority
 *
 * @ORM\Table(name="ticket_priority")
 * @ORM\Entity
 */
class TicketPriority
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=false)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="urgency", type="string", length=255, nullable=false)
     */
    private $urgency;

    /**
     * @var int
     *
     * @ORM\Column(name="sys_insert_user_id", type="integer", nullable=false)
     */
    private $sysInsertUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sys_insert_datetime", type="datetime", nullable=false)
     */
    private $sysInsertDatetime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sys_lastedit_user_id", type="integer", nullable=true)
     */
    private $sysLasteditUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sys_lastedit_datetime", type="datetime", nullable=true)
     */
    private $sysLasteditDatetime;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionCategory
 *
 * @ORM\Table(name="session_category", indexes={@ORM\Index(name="IDX_8DE079A973444FD5", columns={"access_url_id"})})
 * @ORM\Entity
 */
class SessionCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_start", type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_end", type="date", nullable=true)
     */
    private $dateEnd;

    /**
     * @var \AccessUrl
     *
     * @ORM\ManyToOne(targetEntity="AccessUrl")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="access_url_id", referencedColumnName="id")
     * })
     */
    private $accessUrl;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CStudentPublicationRelUser
 *
 * @ORM\Table(name="c_student_publication_rel_user", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="work", columns={"work_id"}), @ORM\Index(name="user", columns={"user_id"})})
 * @ORM\Entity
 */
class CStudentPublicationRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="work_id", type="integer", nullable=false)
     */
    private $workId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;


}

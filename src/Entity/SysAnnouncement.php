<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SysAnnouncement
 *
 * @ORM\Table(name="sys_announcement")
 * @ORM\Entity
 */
class SysAnnouncement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=false)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=false)
     */
    private $dateEnd;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible_teacher", type="boolean", nullable=false)
     */
    private $visibleTeacher;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible_student", type="boolean", nullable=false)
     */
    private $visibleStudent;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible_guest", type="boolean", nullable=false)
     */
    private $visibleGuest;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=250, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=false)
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lang", type="string", length=70, nullable=true)
     */
    private $lang;

    /**
     * @var int
     *
     * @ORM\Column(name="access_url_id", type="integer", nullable=false)
     */
    private $accessUrlId;


}

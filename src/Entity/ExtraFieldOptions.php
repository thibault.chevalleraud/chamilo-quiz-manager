<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExtraFieldOptions
 *
 * @ORM\Table(name="extra_field_options", indexes={@ORM\Index(name="IDX_A572E3AE443707B0", columns={"field_id"})})
 * @ORM\Entity
 */
class ExtraFieldOptions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="option_value", type="text", length=0, nullable=true)
     */
    private $optionValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="display_text", type="string", length=255, nullable=true)
     */
    private $displayText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="priority", type="string", length=255, nullable=true)
     */
    private $priority;

    /**
     * @var string|null
     *
     * @ORM\Column(name="priority_message", type="string", length=255, nullable=true)
     */
    private $priorityMessage;

    /**
     * @var int|null
     *
     * @ORM\Column(name="option_order", type="integer", nullable=true)
     */
    private $optionOrder;

    /**
     * @var \ExtraField
     *
     * @ORM\ManyToOne(targetEntity="ExtraField")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BranchTransaction
 *
 * @ORM\Table(name="branch_transaction", indexes={@ORM\Index(name="IDX_FEFBA12B6BF700BD", columns={"status_id"}), @ORM\Index(name="IDX_FEFBA12BDCD6CC49", columns={"branch_id"})})
 * @ORM\Entity
 */
class BranchTransaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="transaction_id", type="bigint", nullable=false)
     */
    private $transactionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action", type="string", length=20, nullable=true)
     */
    private $action;

    /**
     * @var string|null
     *
     * @ORM\Column(name="item_id", type="string", length=255, nullable=true)
     */
    private $itemId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origin", type="string", length=255, nullable=true)
     */
    private $origin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dest_id", type="string", length=255, nullable=true)
     */
    private $destId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="external_info", type="string", length=255, nullable=true)
     */
    private $externalInfo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_insert", type="datetime", nullable=false)
     */
    private $timeInsert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_update", type="datetime", nullable=false)
     */
    private $timeUpdate;

    /**
     * @var int
     *
     * @ORM\Column(name="failed_attempts", type="integer", nullable=false)
     */
    private $failedAttempts;

    /**
     * @var \BranchTransactionStatus
     *
     * @ORM\ManyToOne(targetEntity="BranchTransactionStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \BranchSync
     *
     * @ORM\ManyToOne(targetEntity="BranchSync")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="branch_id", referencedColumnName="id")
     * })
     */
    private $branch;


}

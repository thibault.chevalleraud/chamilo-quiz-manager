<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuizQuestionRelCategory
 *
 * @ORM\Table(name="c_quiz_question_rel_category", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="idx_qqrc_qid", columns={"question_id"})})
 * @ORM\Entity
 */
class CQuizQuestionRelCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     */
    private $categoryId;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=false)
     */
    private $questionId;

    /**
     * @return int
     */
    public function getIid(): int
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid(int $iid): void
    {
        $this->iid = $iid;
    }

    /**
     * @return int
     */
    public function getCId(): int
    {
        return $this->cId;
    }

    /**
     * @param int $cId
     */
    public function setCId(int $cId): void
    {
        $this->cId = $cId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     */
    public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }


}

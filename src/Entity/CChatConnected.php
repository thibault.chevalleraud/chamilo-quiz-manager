<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CChatConnected
 *
 * @ORM\Table(name="c_chat_connected", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user", columns={"user_id"}), @ORM\Index(name="char_connected_index", columns={"user_id", "session_id", "to_group_id"})})
 * @ORM\Entity
 */
class CChatConnected
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="to_group_id", type="integer", nullable=false)
     */
    private $toGroupId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_connection", type="datetime", nullable=false)
     */
    private $lastConnection;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuizRelCategory
 *
 * @ORM\Table(name="c_quiz_rel_category")
 * @ORM\Entity
 */
class CQuizRelCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var int
     *
     * @ORM\Column(name="exercise_id", type="integer", nullable=false)
     */
    private $exerciseId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="count_questions", type="integer", nullable=true)
     */
    private $countQuestions;


}

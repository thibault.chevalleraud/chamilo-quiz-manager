<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkillRelUser
 *
 * @ORM\Table(name="skill_rel_user", indexes={@ORM\Index(name="IDX_79D3D95AA76ED395", columns={"user_id"}), @ORM\Index(name="IDX_79D3D95A5585C142", columns={"skill_id"}), @ORM\Index(name="IDX_79D3D95A591CC992", columns={"course_id"}), @ORM\Index(name="IDX_79D3D95A613FECDF", columns={"session_id"}), @ORM\Index(name="IDX_79D3D95AF68F11CE", columns={"acquired_level"}), @ORM\Index(name="idx_select_cs", columns={"course_id", "session_id"}), @ORM\Index(name="idx_select_s_c_u", columns={"session_id", "course_id", "user_id"}), @ORM\Index(name="idx_select_sk_u", columns={"skill_id", "user_id"})})
 * @ORM\Entity
 */
class SkillRelUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acquired_skill_at", type="datetime", nullable=false)
     */
    private $acquiredSkillAt;

    /**
     * @var int
     *
     * @ORM\Column(name="assigned_by", type="integer", nullable=false)
     */
    private $assignedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="argumentation", type="text", length=0, nullable=false)
     */
    private $argumentation;

    /**
     * @var int
     *
     * @ORM\Column(name="argumentation_author_id", type="integer", nullable=false)
     */
    private $argumentationAuthorId;

    /**
     * @var \Skill
     *
     * @ORM\ManyToOne(targetEntity="Skill")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="skill_id", referencedColumnName="id")
     * })
     */
    private $skill;

    /**
     * @var \Course
     *
     * @ORM\ManyToOne(targetEntity="Course")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * })
     */
    private $course;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     * })
     */
    private $session;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \SkillLevel
     *
     * @ORM\ManyToOne(targetEntity="SkillLevel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="acquired_level", referencedColumnName="id")
     * })
     */
    private $acquiredLevel;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackEHotspot
 *
 * @ORM\Table(name="track_e_hotspot", indexes={@ORM\Index(name="hotspot_course_code", columns={"hotspot_course_code"}), @ORM\Index(name="hotspot_user_id", columns={"hotspot_user_id"}), @ORM\Index(name="hotspot_exe_id", columns={"hotspot_exe_id"}), @ORM\Index(name="hotspot_question_id", columns={"hotspot_question_id"})})
 * @ORM\Entity
 */
class TrackEHotspot
{
    /**
     * @var int
     *
     * @ORM\Column(name="hotspot_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $hotspotId;

    /**
     * @var int
     *
     * @ORM\Column(name="hotspot_user_id", type="integer", nullable=false)
     */
    private $hotspotUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="hotspot_course_code", type="string", length=50, nullable=false)
     */
    private $hotspotCourseCode;

    /**
     * @var int|null
     *
     * @ORM\Column(name="c_id", type="integer", nullable=true)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="hotspot_exe_id", type="integer", nullable=false)
     */
    private $hotspotExeId;

    /**
     * @var int
     *
     * @ORM\Column(name="hotspot_question_id", type="integer", nullable=false)
     */
    private $hotspotQuestionId;

    /**
     * @var int
     *
     * @ORM\Column(name="hotspot_answer_id", type="integer", nullable=false)
     */
    private $hotspotAnswerId;

    /**
     * @var bool
     *
     * @ORM\Column(name="hotspot_correct", type="boolean", nullable=false)
     */
    private $hotspotCorrect;

    /**
     * @var string
     *
     * @ORM\Column(name="hotspot_coordinate", type="text", length=0, nullable=false)
     */
    private $hotspotCoordinate;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChatVideo
 *
 * @ORM\Table(name="chat_video", indexes={@ORM\Index(name="idx_chat_video_to_user", columns={"to_user"}), @ORM\Index(name="idx_chat_video_from_user", columns={"from_user"}), @ORM\Index(name="idx_chat_video_users", columns={"from_user", "to_user"}), @ORM\Index(name="idx_chat_video_room_name", columns={"room_name"})})
 * @ORM\Entity
 */
class ChatVideo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="from_user", type="integer", nullable=false)
     */
    private $fromUser;

    /**
     * @var int
     *
     * @ORM\Column(name="to_user", type="integer", nullable=false)
     */
    private $toUser;

    /**
     * @var string
     *
     * @ORM\Column(name="room_name", type="string", length=255, nullable=false)
     */
    private $roomName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;


}

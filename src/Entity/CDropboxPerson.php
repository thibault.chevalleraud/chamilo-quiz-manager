<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CDropboxPerson
 *
 * @ORM\Table(name="c_dropbox_person", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user", columns={"user_id"})})
 * @ORM\Entity
 */
class CDropboxPerson
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="file_id", type="integer", nullable=false)
     */
    private $fileId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;


}

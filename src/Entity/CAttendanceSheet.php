<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CAttendanceSheet
 *
 * @ORM\Table(name="c_attendance_sheet", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user", columns={"user_id"}), @ORM\Index(name="presence", columns={"presence"})})
 * @ORM\Entity
 */
class CAttendanceSheet
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var bool
     *
     * @ORM\Column(name="presence", type="boolean", nullable=false)
     */
    private $presence;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="attendance_calendar_id", type="integer", nullable=false)
     */
    private $attendanceCalendarId;


}

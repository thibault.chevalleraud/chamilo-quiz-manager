<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CQuizAnswer
 *
 * @ORM\Table(name="c_quiz_answer", indexes={@ORM\Index(name="c_id", columns={"c_id"}), @ORM\Index(name="idx_cqa_q", columns={"question_id"})})
 * @ORM\Entity
 */
class CQuizAnswer
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="id_auto", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $idAuto;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=false)
     */
    private $questionId;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text", length=0, nullable=false)
     */
    private $answer;

    /**
     * @var int|null
     *
     * @ORM\Column(name="correct", type="integer", nullable=true)
     */
    private $correct;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var float
     *
     * @ORM\Column(name="ponderation", type="float", precision=10, scale=0, nullable=false)
     */
    private $ponderation = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hotspot_coordinates", type="text", length=0, nullable=true)
     */
    private $hotspotCoordinates;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hotspot_type", type="string", length=40, nullable=true)
     */
    private $hotspotType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destination", type="text", length=0, nullable=true)
     */
    private $destination;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answer_code", type="string", length=10, nullable=true)
     */
    private $answerCode;

    /**
     * @return int
     */
    public function getIid(): int
    {
        return $this->iid;
    }

    /**
     * @param int $iid
     */
    public function setIid(int $iid): void
    {
        $this->iid = $iid;
    }

    /**
     * @return int
     */
    public function getIdAuto(): int
    {
        return $this->idAuto;
    }

    /**
     * @param int $idAuto
     */
    public function setIdAuto(int $idAuto): void
    {
        $this->idAuto = $idAuto;
    }

    /**
     * @return int
     */
    public function getCId(): int
    {
        return $this->cId;
    }

    /**
     * @param int $cId
     */
    public function setCId(int $cId): void
    {
        $this->cId = $cId;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     */
    public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     */
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }

    /**
     * @return int|null
     */
    public function getCorrect(): ?int
    {
        return $this->correct;
    }

    /**
     * @param int|null $correct
     */
    public function setCorrect(?int $correct): void
    {
        $this->correct = $correct;
    }

    /**
     * @return null|string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param null|string $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return float
     */
    public function getPonderation(): float
    {
        return $this->ponderation;
    }

    /**
     * @param float $ponderation
     */
    public function setPonderation(float $ponderation): void
    {
        $this->ponderation = $ponderation;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return null|string
     */
    public function getHotspotCoordinates(): ?string
    {
        return $this->hotspotCoordinates;
    }

    /**
     * @param null|string $hotspotCoordinates
     */
    public function setHotspotCoordinates(?string $hotspotCoordinates): void
    {
        $this->hotspotCoordinates = $hotspotCoordinates;
    }

    /**
     * @return null|string
     */
    public function getHotspotType(): ?string
    {
        return $this->hotspotType;
    }

    /**
     * @param null|string $hotspotType
     */
    public function setHotspotType(?string $hotspotType): void
    {
        $this->hotspotType = $hotspotType;
    }

    /**
     * @return null|string
     */
    public function getDestination(): ?string
    {
        return $this->destination;
    }

    /**
     * @param null|string $destination
     */
    public function setDestination(?string $destination): void
    {
        $this->destination = $destination;
    }

    /**
     * @return null|string
     */
    public function getAnswerCode(): ?string
    {
        return $this->answerCode;
    }

    /**
     * @param null|string $answerCode
     */
    public function setAnswerCode(?string $answerCode): void
    {
        $this->answerCode = $answerCode;
    }



}

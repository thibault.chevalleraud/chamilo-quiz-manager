<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceMethod
 *
 * @ORM\Table(name="sequence_method")
 * @ORM\Entity
 */
class SequenceMethod
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="text", length=0, nullable=false)
     */
    private $formula;

    /**
     * @var int
     *
     * @ORM\Column(name="assign", type="integer", nullable=false)
     */
    private $assign;

    /**
     * @var string
     *
     * @ORM\Column(name="met_type", type="string", length=255, nullable=false)
     */
    private $metType;

    /**
     * @var string
     *
     * @ORM\Column(name="act_false", type="string", length=255, nullable=false)
     */
    private $actFalse;


}

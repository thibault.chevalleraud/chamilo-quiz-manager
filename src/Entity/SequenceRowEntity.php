<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceRowEntity
 *
 * @ORM\Table(name="sequence_row_entity", indexes={@ORM\Index(name="IDX_2779761FAED14944", columns={"sequence_type_entity_id"})})
 * @ORM\Entity
 */
class SequenceRowEntity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="session_id", type="integer", nullable=false)
     */
    private $sessionId;

    /**
     * @var int
     *
     * @ORM\Column(name="row_id", type="integer", nullable=false)
     */
    private $rowId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \SequenceTypeEntity
     *
     * @ORM\ManyToOne(targetEntity="SequenceTypeEntity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sequence_type_entity_id", referencedColumnName="id")
     * })
     */
    private $sequenceTypeEntity;


}

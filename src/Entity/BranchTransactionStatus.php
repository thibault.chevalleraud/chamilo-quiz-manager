<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BranchTransactionStatus
 *
 * @ORM\Table(name="branch_transaction_status")
 * @ORM\Entity
 */
class BranchTransactionStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;


}

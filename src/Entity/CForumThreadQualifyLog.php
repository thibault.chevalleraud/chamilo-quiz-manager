<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CForumThreadQualifyLog
 *
 * @ORM\Table(name="c_forum_thread_qualify_log", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="user_id", columns={"user_id", "thread_id"})})
 * @ORM\Entity
 */
class CForumThreadQualifyLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="thread_id", type="integer", nullable=false)
     */
    private $threadId;

    /**
     * @var float
     *
     * @ORM\Column(name="qualify", type="float", precision=10, scale=0, nullable=false)
     */
    private $qualify;

    /**
     * @var int|null
     *
     * @ORM\Column(name="qualify_user_id", type="integer", nullable=true)
     */
    private $qualifyUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="qualify_time", type="datetime", nullable=true)
     */
    private $qualifyTime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;


}

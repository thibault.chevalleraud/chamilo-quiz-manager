<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TicketCategory
 *
 * @ORM\Table(name="ticket_category", indexes={@ORM\Index(name="IDX_8325E540166D1F9C", columns={"project_id"})})
 * @ORM\Entity
 */
class TicketCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="total_tickets", type="integer", nullable=false)
     */
    private $totalTickets;

    /**
     * @var bool
     *
     * @ORM\Column(name="course_required", type="boolean", nullable=false)
     */
    private $courseRequired;

    /**
     * @var int
     *
     * @ORM\Column(name="sys_insert_user_id", type="integer", nullable=false)
     */
    private $sysInsertUserId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sys_insert_datetime", type="datetime", nullable=false)
     */
    private $sysInsertDatetime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sys_lastedit_user_id", type="integer", nullable=true)
     */
    private $sysLasteditUserId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sys_lastedit_datetime", type="datetime", nullable=true)
     */
    private $sysLasteditDatetime;

    /**
     * @var \TicketProject
     *
     * @ORM\ManyToOne(targetEntity="TicketProject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     * })
     */
    private $project;


}

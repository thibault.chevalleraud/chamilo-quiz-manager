<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CLpItem
 *
 * @ORM\Table(name="c_lp_item", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="lp_id", columns={"lp_id"}), @ORM\Index(name="idx_c_lp_item_cid_lp_id", columns={"c_id", "lp_id"})})
 * @ORM\Entity
 */
class CLpItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="lp_id", type="integer", nullable=false)
     */
    private $lpId;

    /**
     * @var string
     *
     * @ORM\Column(name="item_type", type="string", length=32, nullable=false)
     */
    private $itemType;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="text", length=0, nullable=false)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=511, nullable=false)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=511, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text", length=0, nullable=false)
     */
    private $path;

    /**
     * @var float
     *
     * @ORM\Column(name="min_score", type="float", precision=10, scale=0, nullable=false)
     */
    private $minScore;

    /**
     * @var float|null
     *
     * @ORM\Column(name="max_score", type="float", precision=10, scale=0, nullable=true, options={"default"="100"})
     */
    private $maxScore = '100';

    /**
     * @var float|null
     *
     * @ORM\Column(name="mastery_score", type="float", precision=10, scale=0, nullable=true)
     */
    private $masteryScore;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_item_id", type="integer", nullable=false)
     */
    private $parentItemId;

    /**
     * @var int
     *
     * @ORM\Column(name="previous_item_id", type="integer", nullable=false)
     */
    private $previousItemId;

    /**
     * @var int
     *
     * @ORM\Column(name="next_item_id", type="integer", nullable=false)
     */
    private $nextItemId;

    /**
     * @var int
     *
     * @ORM\Column(name="display_order", type="integer", nullable=false)
     */
    private $displayOrder;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prerequisite", type="text", length=0, nullable=true)
     */
    private $prerequisite;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parameters", type="text", length=0, nullable=true)
     */
    private $parameters;

    /**
     * @var string
     *
     * @ORM\Column(name="launch_data", type="text", length=0, nullable=false)
     */
    private $launchData;

    /**
     * @var string|null
     *
     * @ORM\Column(name="max_time_allowed", type="string", length=13, nullable=true)
     */
    private $maxTimeAllowed;

    /**
     * @var string|null
     *
     * @ORM\Column(name="terms", type="text", length=0, nullable=true)
     */
    private $terms;

    /**
     * @var int|null
     *
     * @ORM\Column(name="search_did", type="integer", nullable=true)
     */
    private $searchDid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="audio", type="string", length=250, nullable=true)
     */
    private $audio;

    /**
     * @var float|null
     *
     * @ORM\Column(name="prerequisite_min_score", type="float", precision=10, scale=0, nullable=true)
     */
    private $prerequisiteMinScore;

    /**
     * @var float|null
     *
     * @ORM\Column(name="prerequisite_max_score", type="float", precision=10, scale=0, nullable=true)
     */
    private $prerequisiteMaxScore;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CForumThread
 *
 * @ORM\Table(name="c_forum_thread", indexes={@ORM\Index(name="course", columns={"c_id"}), @ORM\Index(name="idx_forum_thread_forum_id", columns={"forum_id"})})
 * @ORM\Entity
 */
class CForumThread
{
    /**
     * @var int
     *
     * @ORM\Column(name="iid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iid;

    /**
     * @var int
     *
     * @ORM\Column(name="c_id", type="integer", nullable=false)
     */
    private $cId;

    /**
     * @var int
     *
     * @ORM\Column(name="thread_id", type="integer", nullable=false)
     */
    private $threadId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="thread_title", type="string", length=255, nullable=true)
     */
    private $threadTitle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="forum_id", type="integer", nullable=true)
     */
    private $forumId;

    /**
     * @var int
     *
     * @ORM\Column(name="thread_replies", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $threadReplies = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="thread_poster_id", type="integer", nullable=true)
     */
    private $threadPosterId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="thread_poster_name", type="string", length=100, nullable=true)
     */
    private $threadPosterName;

    /**
     * @var int
     *
     * @ORM\Column(name="thread_views", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $threadViews = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="thread_last_post", type="integer", nullable=true)
     */
    private $threadLastPost;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thread_date", type="datetime", nullable=true)
     */
    private $threadDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="thread_sticky", type="boolean", nullable=true)
     */
    private $threadSticky;

    /**
     * @var int
     *
     * @ORM\Column(name="locked", type="integer", nullable=false)
     */
    private $locked;

    /**
     * @var int|null
     *
     * @ORM\Column(name="session_id", type="integer", nullable=true)
     */
    private $sessionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="thread_title_qualify", type="string", length=255, nullable=true)
     */
    private $threadTitleQualify;

    /**
     * @var float
     *
     * @ORM\Column(name="thread_qualify_max", type="float", precision=10, scale=0, nullable=false)
     */
    private $threadQualifyMax;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="thread_close_date", type="datetime", nullable=true)
     */
    private $threadCloseDate;

    /**
     * @var float
     *
     * @ORM\Column(name="thread_weight", type="float", precision=10, scale=0, nullable=false)
     */
    private $threadWeight;

    /**
     * @var bool
     *
     * @ORM\Column(name="thread_peer_qualify", type="boolean", nullable=false)
     */
    private $threadPeerQualify;

    /**
     * @var int
     *
     * @ORM\Column(name="lp_item_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $lpItemId;


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SharedSurveyQuestionOption
 *
 * @ORM\Table(name="shared_survey_question_option")
 * @ORM\Entity
 */
class SharedSurveyQuestionOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="question_option_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $questionOptionId;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=false)
     */
    private $questionId;

    /**
     * @var int
     *
     * @ORM\Column(name="survey_id", type="integer", nullable=false)
     */
    private $surveyId;

    /**
     * @var string
     *
     * @ORM\Column(name="option_text", type="text", length=0, nullable=false)
     */
    private $optionText;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     */
    private $sort;


}

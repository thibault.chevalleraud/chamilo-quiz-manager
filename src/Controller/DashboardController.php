<?php
    namespace App\Controller;
        
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    
    class DashboardController extends Controller {
    
        public function index(){
            return $this->render('dashboard/index.html.twig');
        }
    
    }
?>
<?php
    namespace App\Controller;
        
    use App\Entity\Course;
    use App\Entity\CQuiz;
    use App\Entity\CQuizAnswer;
    use App\Entity\CQuizQuestion;
    use App\Entity\CQuizRelQuestion;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\HttpFoundation\Request;

    class QuizController extends Controller {
    
        public function index(){
            $em = $this->getDoctrine()
                       ->getManager();

            $course = $em->getRepository(Course::class)
                         ->findAll();

            $quiz = $em->getRepository(CQuiz::class)
                       ->findAll();

            return $this->render('quiz/index.html.twig', array(
                'course'    => $course,
                'quiz'      => $quiz
            ));
        }

        public function import($cid, $qid, Request $request){
            $em = $this->getDoctrine()
                       ->getManager();

            $course = $em->getRepository(Course::class)
                         ->findAll();

            $form = $this->createFormBuilder()
                         ->add('save', SubmitType::class, array('label' => 'Create Task'))
                         ->getForm();

            $form->handleRequest($request);
            if($form->isSubmitted()){
                $srcQuizId      = $qid;
                $srcCourseId    = $cid;
                $destCourseId   = $request->request->get('dest');
                $destQuizId     = $this->import_c_quiz($srcQuizId, $destCourseId);
                $this->import_c_quiz_rel_question($srcCourseId, $srcQuizId, $destCourseId, $destQuizId);
                return $this->redirectToRoute('quiz_index');
            }

            return $this->render('quiz/import.html.twig', array(
                'course'    => $course,
                'form'      => $form->createView()
            ));
        }

        public function random($cid, $qid, Request $request){
            $em = $this->getDoctrine()
                ->getManager();

            $quiz = $em->getRepository(CQuiz::class)
                       ->findBy(array('cId'=>$cid));

            $form = $this->createFormBuilder()
                ->add('save', SubmitType::class, array('label' => 'Create Task'))
                ->getForm();

            $form->handleRequest($request);
            if($form->isSubmitted()){
                $courseId   = $cid;
                $srcQuizId  = $qid;
                $destQuizId = $request->request->get('dest');
                $qte        = (int)$request->request->get('qte');

                $questions = $em->getRepository(CQuizRelQuestion::class)
                                ->findBy(array('cId'=>$courseId, 'exerciceId'=>$srcQuizId));

                if(sizeof($questions) >= $qte){
                    $rand = array_rand($questions, $qte);
                    foreach ($rand as $k => $v){
                        $question = new CQuizRelQuestion();
                        $question->setCId($courseId);
                        $question->setQuestionOrder($k+1);
                        $question->setQuestionId($questions[$v]->getQuestionId());
                        $question->setExerciceId($destQuizId);
                        $em->persist($question);
                        $em->flush();
                    }
                    return $this->redirectToRoute('quiz_index');
                } else {
                    die('erreur : le quiz ne fait pas '.$qte.' questions');
                }
                die();
                return $this->redirectToRoute('quiz_index');
            }

            return $this->render('quiz/random.html.twig', array(
                'quiz'    => $quiz,
                'form'      => $form->createView()
            ));
        }

        public function delete($id){
            $em = $this->getDoctrine()
                ->getManager();

            /**
             * Suppression dans c_quiz
             */
                $quiz = $em->getRepository(CQuiz::class)
                           ->find($id);

            /**
             * Suppression dans c_quiz_rel_question
             */
                $relQuestions = $em->getRepository(CQuizRelQuestion::class)
                                   ->findBy(array('exerciceId'=>$id));
                foreach ($relQuestions as $rq){
                    $question = $em->getRepository(CQuizQuestion::class)
                                   ->find($rq->getQuestionId());
                    $answers = $em->getRepository(CQuizAnswer::class)
                                  ->findBy(array('questionId'=>$rq->getQuestionId()));
                    foreach ($answers as $a){
                        $em->remove($a);
                    }
                    $em->remove($question);
                    $em->remove($rq);
                }
                $em->remove($quiz);
                $em->flush();
            return $this->redirectToRoute('quiz_index');
        }

        private function import_c_quiz($srcQuizID, $destCourseId){
            $em = $this->getDoctrine()
                       ->getManager();
            $quiz = $em->getRepository(CQuiz::class)
                       ->find($srcQuizID);
            $destQuiz = new CQuiz();
            $destQuiz->setCId($destCourseId);
            $destQuiz->setTitle($quiz->getTitle());
            $destQuiz->setType($quiz->isType());
            $destQuiz->setRandom($quiz->getRandom());
            $destQuiz->setRandomAnswers($quiz->isRandomAnswers());
            $destQuiz->setActive(false);
            $destQuiz->setResultsDisabled($quiz->getResultsDisabled());
            $destQuiz->setMaxAttempt($quiz->getMaxAttempt());
            $destQuiz->setFeedbackType($quiz->getFeedbackType());
            $destQuiz->setExpiredTime($quiz->getExpiredTime());
            $destQuiz->setPropagateNeg($quiz->getPropagateNeg());
            $destQuiz->setSaveCorrectAnswers($quiz->isSaveCorrectAnswers());
            $destQuiz->setReviewAnswers($quiz->getReviewAnswers());
            $destQuiz->setRandomByCategory($quiz->getRandomByCategory());
            $destQuiz->setDisplayCategoryName($quiz->getDisplayCategoryName());
            $em->persist($destQuiz);
            $em->flush();
            $destQuiz->setId($destQuiz->getIid());
            $em->persist($destQuiz);
            $em->flush();
            return $destQuiz->getId();
        }

        private function import_c_quiz_rel_question($srcCourseId, $srcQuizId, $destCourseId, $destQuizId){
            $order = 1;
            $em = $this->getDoctrine()
                       ->getManager();

            $QuizRelQuestions = $em->getRepository(CQuizRelQuestion::class)
                                   ->findBy(array('cId'=>$srcCourseId, 'exerciceId'=>$srcQuizId));

            foreach ($QuizRelQuestions as $v){
                $destQuestionId = $this->import_c_quiz_question($v->getQuestionId(), $destCourseId);
                $this->import_c_quiz_answer($srcCourseId, $v->getQuestionId(), $destCourseId, $destQuestionId);

                $data = new CQuizRelQuestion();
                $data->setCId($destCourseId);
                $data->setExerciceId($destQuizId);
                $data->setQuestionOrder($order);
                $data->setQuestionId($destQuestionId);

                $em->persist($data);
                $em->flush();
                $order++;
            }
        }

        private function import_c_quiz_question($srcQuestionId, $destCourseId){
            $em = $this->getDoctrine()
                       ->getManager();
            $question = $em->getRepository(CQuizQuestion::class)
                           ->find($srcQuestionId);
            $destQuestion = new CQuizQuestion();
            $destQuestion->setCId($destCourseId);
            $destQuestion->setQuestion($question->getQuestion());
            $destQuestion->setDescription($question->getDescription());
            $destQuestion->setPonderation($question->getPonderation());
            $destQuestion->setPosition($question->getPosition());
            $destQuestion->setType($question->getType());
            $destQuestion->setPicture($question->getPicture());
            $destQuestion->setLevel($question->getLevel());
            $em->persist($destQuestion);
            $em->flush();
            $destQuestionId = $destQuestion->getIid();
            $destQuestion->setId($destQuestionId);
            $em->persist($destQuestion);
            $em->flush();
            return $destQuestionId;
        }

        private function import_c_quiz_answer($srcCourseId, $srcQuestionId, $destCourseId, $destQuestionId){
            $em = $this->getDoctrine()
                       ->getManager();

            $answers = $em->getRepository(CQuizAnswer::class)
                          ->findBy(array('cId'=>$srcCourseId, 'questionId'=>$srcQuestionId));

            foreach ($answers as $w){
                $answer = new CQuizAnswer();
                $answer->setQuestionId($destQuestionId);
                $answer->setCId($destCourseId);
                $answer->setIdAuto(0);
                $answer->setAnswer($w->getAnswer());
                $answer->setCorrect($w->getCorrect());
                $answer->setComment($w->getComment());
                $answer->setPosition($w->getPosition());
                $answer->setPonderation($w->getPonderation());
                $answer->setDestination($w->getDestination());
                $em->persist($answer);
                $em->flush();
                $answer->setIdAuto($answer->getIid());
                $answer->setId($answer->getIid());
                $em->persist($answer);
                $em->flush();
            }
        }
    
    }
?>